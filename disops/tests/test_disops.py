import disops
from sympy import Symbol


class TestDisops:
    ''' Test class for disops library

    Warning: These tests may take a while.
    '''


    def check(generator, npol=15):
        generator.clear()
        U = generator.gen_ops(npol)

        generator.clear()
        V = generator.sgen_ops(npol)
        
        assert [u.as_expr() for u in U] == [v.as_expr() for v in V]


    def test_Charlier(self):
        TestDisops.check(disops.Charlier(Symbol('m')), npol=20)


    def test_Meixner(self):
        TestDisops.check(disops.Meixner(Symbol('b'), Symbol('c')), npol=20)


    def test_Hahn_1(self):
        TestDisops.check(disops.Hahn(50, Symbol('a'), Symbol('b')), npol=20)
    

    def test_Hahn_2(self):
        TestDisops.check(disops.Hahn(80, Symbol('a'), Symbol('b')), npol=20)


    def test_Krawtchouk_1(self):
        TestDisops.check(disops.Krawtchouk(30, Symbol('p')), npol=30)
    

    def test_Krawchouk_2(self):
        TestDisops.check(disops.Krawtchouk(80, Symbol('p')), npol=20)