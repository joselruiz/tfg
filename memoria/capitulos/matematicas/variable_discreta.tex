% !TeX root = ../libro.tex
% !TeX encoding = utf8

%\setchapterpreamble[c][0.75\linewidth]{%
%	\sffamily
%  Breve resumen del capítulo. TODO
%	\par\bigskip
%}
\chapter{Polinomios Ortogonales de Variable Discreta}\label{ch:segundo-capitulo}
El objetivo de este capítulo es construir familias de polinomios ortogonales de variable discreta y describir las familias clásicas de variable discreta que aparecen en la literatura. A partir de ahora, nos centraremos únicamente en funcionales de momentos reales definidos sobre el espacio de polinomios reales, o sea, $\funl: \R[x] \longrightarrow \R$. Estos funcionales son un caso particular de los del \autoref{ch:primer-capitulo}, por lo que está claro verifican las propiedades estudiadas en el mismo, de las que haremos uso.

Para la elaboración de este capítulo se ha utilizado principalmente \cite{Renato}, \cite{Chihara}, \cite{Niki} y \cite{Koekoek}.

\section{SPO de variable continua y variable discreta}\label{sec:2.1}
    Comenzamos la sección recordando una definición elemental:
    \begin{definicion}
        \cite[p. ~137]{Monier}
        Sea $E$ un espacio vectorial real. La aplicación $\phi: E \times E \longrightarrow \R$ es un \emph{producto escalar} si verifica:
        \begin{enumerate}
            \item $\phi$ es simétrica: $\phi(x, y) = \phi(y, x)$, $\forall x, y \in E$.
            
            \item $\phi$ es bilineal: $\phi(\lambda x + z, y) = \lambda \phi(x, y) + \phi(z, y)$, $\forall x, y, z \in E$, $\forall \lambda \in \R$. \footnote{De acuerdo con la condición escrita, $\phi$ sería bilineal únicamente en la primera variable, pero basta aplicar la simetría para obtener la bilinealidad en la segunda variable y por ello podemos afirmar simplemente que $\phi$ es bilineal.}
            
            
            \item $\phi$ es definida positiva: $\phi(x, x) > 0$, $\forall x \neq 0$.
            
            \item $\phi$ es no degenerada: Si $x \in E$ y $\phi(x, x) = 0$, entonces $x = 0$.
        \end{enumerate}
    \end{definicion}

    \begin{observacion}
        Usualmente, y de forma más tediosa, se escribe la condición de bilinealidad como:
        \begin{equation}
            \phi(\lambda x + \mu z, y) = \lambda \phi(x, y) + \mu \phi(z, y), \quad \forall x, y, z \in E, \ \forall \lambda, \mu \in \R
        \end{equation}
        Veamos que ambas formas son equivalentes. Sean $x, y, z \in E$ y $\lambda, \mu \in \R$.
        \begin{itemize}
            \item Supongamos que $\phi(\lambda x + z, y) = \lambda \phi(x, y) + \phi(z, y)$. Entonces,
                \begin{align}
                    \phi(\lambda x + \mu z, y) &= \lambda \phi(x, y) + \phi(\mu z, y) \\
                    &= \lambda \phi(x, y) + \phi(\mu z + 0, y) \\
                    &= \lambda \phi(x, y) + \mu \phi(z, y) + \phi(0, y).
                \end{align}
                Basta con que se anule el último sumando y la implicación quedará probada. En efecto,
                \begin{equation}
                    \phi(0, y) = \phi(-y + y, y) = - \phi(y, y) + \phi (y, y) = 0.
                \end{equation}
            \item Recíprocamente, si $\phi(\lambda x + \mu z, y) = \lambda \phi(x, y) + \mu \phi(z, y)$, entonces:
                \begin{equation}
                    \phi(\lambda x + z, y) = \phi(\lambda x + 1z, y) = \lambda \phi(x, y) + 1 \phi(z, y) = \lambda \phi(x, y) + \phi(z, y) .
                \end{equation}
        \end{itemize}
            \begin{flushright} $\square$ \end{flushright} % fin de la mini demostración dentro de una observación.
    \end{observacion}

    En la siguiente proposición vemos cómo un funcional de momentos puede inducir un producto escalar.
    \begin{proposicion}\label{th:producto_escalar}
        Sea $\funl$ un funcional de momentos definido positivo. La aplicación 
        \begin{equation}
            \begin{split}
                & \pescalar{\cdot}{\cdot} : \R[x] \times \R[x] \longrightarrow \R \\
                & \pescalar{P}{Q} = \funl[PQ], \quad \forall P, Q \in \R[x]
            \end{split}
        \end{equation}
        es un producto escalar.
    \end{proposicion}
    \begin{proof}
        Sean $P, Q, R \in \R[x]$ y $\lambda \in \R$ arbitrarios.
        \begin{enumerate}
            \item $\pescalar{P}{Q} = \funl [PQ] = \funl [QP] = \pescalar{Q}{P}$.
            
            \item $\pescalar{\lambda P + R}{Q} = \funl [(\lambda P + R)Q] = \funl[\lambda P Q + RQ] = \lambda \funl[PQ] + \funl[RQ] = \lambda \pescalar{P}{Q} + \pescalar{R}{Q}$, \\
            $\pescalar{P}{\lambda Q + R} = \funl [P(\lambda Q + R)] = \funl[\lambda P Q + PR] = \lambda \funl[PQ] + \funl[PR] = \lambda \pescalar{P}{Q} + \pescalar{P}{R}$.
            
            
            \item Supongamos adicionalmente que $P$ no es nulo y aplicamos que $\funl$ es definido positivo: $\pescalar{P}{P} = \funl [P^2] \in \R^+$.

            \item Si $\pescalar{P}{P} = \funl[P^2] = 0$, como $\funl$ es definido positivo, necesariamente $P = 0$.
        \end{enumerate}
    \end{proof}

    \begin{definicion}
        A la aplicación de la Proposición \ref{th:producto_escalar} la llamaremos \emph{producto escalar asociado a $\funl$}.
    \end{definicion}

    Ahora podemos reescribir la Definición \ref{def:spo}, en términos del producto escalar.
    \begin{definicion}
        Sea $\pescalar{\cdot}{\cdot}$ un producto escalar asociado a algún funcional de momentos.
        Una \emph{sucesión de polinomios ortogonales (SPO)} con respecto a $\pescalar{\cdot}{\cdot}$ es una sucesión $\spol$ tal que para cualesquiera $m, n \in \N_0$ verifica:
        \begin{enumerate}
        \item $P_n$ es un polinomio de grado $n$.
        \item $\pescalar{P_m}{P_n} = 0$ si $m \neq n$.
        \item $\pescalar{P_n}{P_n} > 0$.
        \end{enumerate}
    \end{definicion}

    Según la forma que adopte un producto escalar, podemos encontrar dos grandes familias de SPO: de variable continua y de variable discreta; según procedan, respectivamente, de productos escalares continuos o discretos. \footnote{Existen SPO que no se ajustan a ninguna de estas dos posibilidades y quedan fueran del estudio de este trabajo.}

    Intuitivamente, para calcular un producto escalar continuo, digamos de dos polinomios $P$ y $Q$, tenemos en cuenta todos los valores que toman $P$ y $Q$. Por contra, si queremos calcular un producto escalar discreto sólo utilizamos el valor de $P$ y de $Q$ en algunos puntos concretos de su dominio. Así, parece razonable definir los productos escalares continuos mediante integrales y los discretos, mediante sumatorias.

    \begin{definicion}
        \cite[p. ~150]{Stoer}
        Sea $a, b \in \R \cup \{-\infty, \infty\}$, $a < b$, $w: [a, b] \longrightarrow \R$ y 
        \begin{equation}
            \mu_k = \int_a^b x^k w(x) dx .
        \end{equation}
        $w$ es una \emph{función peso} si verifica:
        \begin{enumerate}
            \item $w(x) \geq 0$, $\forall x \in [a, b]$.
            \item $w$ es medible.
            \item $\mu_k < \infty$, $\forall k \in \N_0$.
            \item Si $P \in \R[x]$, con $P(x) \geq 0$, $\forall x \in [a, b]$, y $\int_a^b P(x)w(x) = 0$, entonces $P(x) = 0$, $\forall x \in [a, b]$.
        \end{enumerate}
        A cada $\mu_k$ lo llamamos momento de orden $k$ asociado a la función peso $w$.
    \end{definicion}

    \begin{proposicion} \label{pr:producto_continuo}
        Sea $w : [a, b] \longrightarrow \R$ una función peso.
        Entonces,
        \begin{equation}
            \pescalar{P}{Q} = \int_a^b P(x) Q(x) w(x) dx, \quad \forall P, Q \in \R[x],
        \end{equation}
        es un producto escalar.
    \end{proposicion}
    \begin{proof}
        Definimos el funcional de momentos $\funl: \R[x] \longrightarrow \R$, dado por
        \begin{equation}
            \funl[x^k] =  \mu_k = \int_a^b x^k w(x) dx, \quad \forall k \in \N_0
        \end{equation}
        y extendido por linealidad. $\funl$ es claramente definido positivo y $\funl[PQ] = \pescalar{P}{Q}$, $\forall P, Q \in \R[x]$. Entonces, por la Proposición \ref{th:producto_escalar}, $\pescalar{\cdot}{\cdot}$ es un producto escalar.
    \end{proof}
    \begin{definicion}
        \cite[p. ~18]{Niki}
        A un producto escalar definido como en la Proposición \ref{pr:producto_continuo} lo llamamos \emph{producto escalar continuo} y a una SPO con respecto al mismo, \emph{sucesión de polinomios ortogonales de variable continua (SPOVC)}.
    \end{definicion}

    Las sucesiones de polinomios ortogonales de variable continua son bien conocidas en la Físico-Matemática; en particular, las clásicas, debido a sus propiedades diferenciales que las caracterizan. Recogemos sus denominaciones y las funciones peso que las determinan en la siguiente tabla. \cite{Renato}
    \begin{table}[H]
        \label{tabla_continuas}
        \begin{tabular}{lccc}\toprule
            \multicolumn{1}{c}{\textbf{Familia}}  & \textbf{Parámetros} & \textbf{$w(x)$}           & \textbf{Intervalo de integración} \\ \midrule
            \textbf{Hermite}                      &                     & $e^{-x^2}$                & $\mathbb R$             \\
            \textbf{Laguerre}                     & $\alpha>-1$         & $x^\alpha e^{-x}$         & $[0, \infty]$           \\
            \textbf{Legendre}                     &                     & 1                         & $[-1, 1]$               \\
            \textbf{Chebyshev de primera especie} &                     & $(1-x^2)^{-1/2}$          & $[-1, 1]$               \\
            \textbf{Cebyshev de segunda especie}  &                     & $\sqrt{1-x^2}$            & $[-1, 1]$              \\ \bottomrule
            \end{tabular}
        \caption{Familias clásicas de polinomios ortogonales de variable continua}
    \end{table}
    \begin{observacion}
    Las tres últimas familias de la tabla \ref{tabla_continuas} son casos particulares de los polinomios de Jacobi, cuya función peso es $w(x) = (1-x)^\alpha (1+x)^\beta$, dependiente de los parámetros $\alpha, \beta \in \R$. Si $\alpha=\beta=0$, obtenemos los polinomios de Lengendre; si $\alpha=\beta=-1/2$, los de Chebyshev de primera especie; y si $\alpha=\beta=1/2$, los de Chebyshev de segunda especie.
    \end{observacion}

    \begin{comment}
    \begin{definicion}
        Sea $X \subset \R$ un conjunto numerable finito o infinito, $w: X \longrightarrow \R$ y 
        \begin{equation}
            \mu_k = \sum_{x \in X} x^k w(x).
        \end{equation}
        $w$ es una \emph{función peso de variable discreta} si verifica:
        \begin{enumerate}
            \item $w(x) \geq 0$, $\forall x \in X$.
            \item $w$ es medible.
            \item $\mu_k < \infty$, $\forall k \in \N_0$.
            \item Si $P \in \R[x]$, con $P(x) \geq 0$, $\forall x \in X$, y $\sum_{x \in X} P(x)w(x) = 0$, entonces $P(x) = 0$, $\forall x \in X$.
        \end{enumerate}
        A cada $\mu_k$ lo llamamos momento de orden $k$ asociado a la función peso $w$.
    \end{definicion}
    
    \begin{definicion}
        Sea $X \subset \R$ un conjunto numerable finito o infinito. Diremos que una función $w: X \longrightarrow \R$ es una \emph{función peso de variable discreta} si es la restricción a $X$ de una función peso, es decir, si existe una función peso $W: [a, b] \longrightarrow \R$, con $X \subset [a, b] \subset [-\infty, \infty]$, tal que $w = W_{\vert X}$.
    \end{definicion}
    \end{comment}

    \begin{proposicion} \label{pr:producto_discreto}
        Sea $w: [a, b] \longrightarrow \R$ una función peso y $X$ un conjunto numerable, finito o infinito, con $X \subset [a, b]$. Entonces,
        \begin{equation}
            \pescalar{P}{Q} = \sum_{x \in X} P(x) Q(x) w(x), \quad \forall P, Q \in \R[x], 
        \end{equation}
        es un producto escalar definido sobre $X$.
    \end{proposicion}
    \begin{proof}
        Definimos el funcional de momentos $\funl: \R[x] \longrightarrow \R$, dado por $\funl[x^k] =  \mu_k$, $\forall k \in \N_0$, y extendido por linealidad. $\funl$ es claramente definido positivo y $\funl[PQ] = \pescalar{P}{Q}$, $\forall P, Q \in \R[x]$. Entonces, por la Proposición \ref{th:producto_escalar}, $\pescalar{\cdot}{\cdot}$ es un producto escalar. \footnote{En realidad, a la hora de comprobar la propiedad 4 del producto escalar, todo lo que podemos asegurar bajo las hipótesis es que $P(x) = 0$, $\forall x \in X$, sin tener porqué ser idénticamente nulo. Sin embargo, cuando tratamos con productos escalares discretos, consideramos como nulo cualquier polinomio $P \in \R[x]$ que cumpla $P(x) = 0$, $\forall x \in X$, y obtenemos una teoría coherente.}
    \end{proof}
    \begin{definicion}
        \cite[p. ~18]{Niki}
        A un producto escalar definido como en la Proposición \ref{pr:producto_discreto} lo llamamos \emph{producto escalar discreto} y a una SPO con respecto al mismo, \emph{sucesión de polinomios ortogonales de variable discreta (SPOVD)}.
    \end{definicion}


\section{Propiedades de las SPOVD}\label{sec:2.2}
    Para las siguientes dos definiciones, en las que introduciremos los operadores en diferencias finitas, consideraremos $\mathcal F$ el espacio de funciones reales de variable real.

    \begin{definicion}
        \cite[p. ~104]{Renato}
        El \emph{operador en diferencias finitas progresivas} es la aplicación
        \begin{align}
            \Delta : \mathcal F \longrightarrow \mathcal F \\
            f \longmapsto \Delta f ,
        \end{align}
        con
        \begin{equation}
            \Delta f(x) = f(x+1) - f(x), \quad \forall x \in \R .
        \end{equation}
    \end{definicion}

    \begin{definicion}
        \cite[p. ~104]{Renato}
        El \emph{operador en diferencias finitas regresivas} es la aplicación
        \begin{align}
            \nabla : \mathcal F \longrightarrow \mathcal F \\
            f \longmapsto \nabla f ,
        \end{align}
        con
        \begin{equation}
            \nabla f(x) = f(x) - f(x-1), \quad \forall x \in \R .
        \end{equation}
    \end{definicion}

    \begin{observacion}
        \begin{equation}
            \Delta f(x) = f(x+1) - f(x) = \nabla f(x+1), \quad \forall f \in \mathcal F, \ \forall x \in \R .
        \end{equation}
    \end{observacion}

    \begin{definicion}
        \cite[p. ~104]{Renato}
        La \emph{ecuación en diferencias hipergeométrica} es la ecuación
        \begin{equation}
            \sigma(x)   \Delta \nabla y(x) + \tau(x) \Delta y(x) + \lambda y(x) = 0,
        \end{equation}
        cuya incógnita es la función real de variable real $y$, $\sigma$ y $\tau$ son polinomios de grado a lo sumo 2 y 1, respectivamente, y $\lambda \in \R$.
    \end{definicion}
    De esta ecuación nos interesarán las soluciones polinómicas, que estarán determinadas por la  elección que hagamos de $\sigma$ y $\tau$.

    \begin{definicion}
        \cite[p. ~117]{Renato}
        Sea $\sigma$ y $\tau$ dos polinomios de grado a lo sumo 2 y exactamente 1, respectivamente, con raíces reales y distintas y sea $\rho$ una función peso tal que
        \begin{equation}
            \Delta[\sigma(x) \rho(x)] = \tau(x) \rho(x), \quad \sigma(x) \rho(x) x^k \vert_a^b = 0, \ \forall k \in \N_0,
        \end{equation}
        donde $]a, b[$ es un intervalo en el que está definida $\rho$. Diremos que una familia de polinomios ortogonales $\spol$ es \emph{clásica discreta} si dicha familia es ortogonal respecto a la función $\rho$ solución de la ecuación anterior. \footnote{$\sigma(x) \rho(x) x^k \vert_a^b = \lim_{x \to b}\sigma(x) \rho(x) x^k - \lim_{x \to a}\sigma(x) \rho(x) x^k$}
    \end{definicion}
    
    \begin{teorema}
        \cite[p. ~117]{Renato}
        Equivalen las siguientes afirmaciones:
        \begin{enumerate}
            \item $\spol$ es una familia clásica discreta.
            \item $\spol$ es solución de la ecuación en diferencias hipergeométrica
            \begin{equation} \label{eq:ecuacion_dif_hiper_teorema}
                \sigma(x) \Delta \nabla P_n(x) + \tau(x) \Delta P_n(x) + \lambda_n P_n(x) = 0, \quad \forall n \in \N_0 .
            \end{equation}

            \item $\spol$ se expresa mediante la fórmula de Rodrigues
            \begin{equation}
                P_n(x) = \dfrac{B_n}{\rho(x)} \Delta^n [\rho(x+n) \prod_{k=1}^n \sigma(x+k)], \quad \forall n \in \N_0 .
            \end{equation}
            
        \end{enumerate}
    \end{teorema}
    
    Así pues, resolviendo la ecuación \refeq{eq:ecuacion_dif_hiper_teorema} para valores fijados de $\sigma$, $\tau$ y $\lambda_n$, obtendremos las sucesiones de polinomios ortogonales clásicas discretas que buscamos en este capítulo: Charlier, Meixner, Krawtchouk y Hahn.

\section{Familias clásicas de Polinomios Ortogonales de Variable Discreta}\label{sec:2.3}
    Empezamos con unas definiciones elementales, que si bien no pertenecen de forma exclusiva al campo de los \emph{Polinomios Ortogonales}, aparecen con frecuencia y conviene recordarlas.
    \begin{definicion}
        \cite[p. ~18]{Seaborn}
        Para cada $z \in \C$ definimos su \emph{símbolo de Pochhammer}, $(z)_n$ por:
        \begin{equation}
            (z)_n =
            \begin{cases}
                1 \quad & \text{si } n = 0, \\
                z(z+1)(z+2)(z+3)\cdots(z+n-1) \quad & \text{si } n \in \N.
            \end{cases}
        \end{equation}
    \end{definicion}

    \begin{definicion}
        \cite[p. ~855]{Davis}
        La \emph{función gamma} es la función $\Gamma: \R \longrightarrow \R$ dada por
        \begin{equation}
            \Gamma(x) = \int_0^\infty e^{-t} t^{x-1} dt .
        \end{equation}
    \end{definicion}
    \begin{observacion}
        \cite[p. ~855]{Davis}
        Si $x \in \R^+$, entonces
        \begin{equation}
            \Gamma(x+1) = x \Gamma(x).
        \end{equation}
        Además, si $n \in \N_0$,
        \begin{equation}
            \Gamma(n+1) = n!
        \end{equation}
    \end{observacion}

    \begin{definicion}
        Si $x, y$ son dos números reales positivos, definimos el número combinatorio generalizado
        \begin{equation}
            \binom{x}{y} = \frac{\Gamma(x+1)}{\Gamma(y+1) \Gamma(x-y+1)}.
        \end{equation}
    \end{definicion}
    \begin{observacion}
        Si $x, y \in \N_0$, $x \geq y$, entonces la definición anterior coincide con la definición clásica de número combinatorio, es decir,
        \begin{equation}
            \binom{x}{y} = \frac{x!}{y! (x-y)!}.
        \end{equation}
    \end{observacion}
    \begin{observacion} \label{ob:combinatorio}
        Si $x \in \R$ y $n \in \N_0$, podemos obtener una expresión más sencilla del número combinarorio:
        \begin{align}
            \binom{x}{n} &= \frac{\Gamma(x+1)}{\Gamma(n+1) \Gamma(x-n+1)} = \frac{\Gamma(x+1)}{n! \Gamma(x-n+1)} = \frac{x\Gamma(x)}{n! \Gamma(x-n+1)} = \frac{x(x-1)\Gamma(x-1)}{n! \Gamma(x-n+1)} = \\
            &= \frac{x(x-1)(x-2)\Gamma(x-2)}{n! \Gamma(x-n+1)} = \dots = \frac{x(x-1)(x-2) \cdots (x-n+1) \Gamma(x-n+1)}{n! \Gamma(x-n+1)} = \\
            &= \frac{x(x-1)(x-2) \cdots (x-n+1)}{n!} = \frac{(x-n+1)_n}{n!} .
        \end{align}
    \end{observacion}

    \subsection{Polinomios de Charlier}

    \begin{definicion}
        \cite[p. ~170]{Chihara}
        Los \emph{polinomios de Charlier} $\charlier$, de parámetro $\mu \in \R^+$, son la SPOVD con respecto al funcional de momentos
        \begin{equation}
            \funl[P] = e^{-\mu} \sum_{x \in \N_0} P(x) \frac{\mu^x}{x!},  \quad \forall P \in \R[x].
        \end{equation}
        O, equivalentemente, con respecto al producto escalar
        \begin{equation}
            \pescalar{P}{Q}_C = e^{-\mu} \sum_{x \in \N_0} P(x) Q(x) \frac{\mu^x}{x!}, \quad \forall P, Q \in \R[x].
        \end{equation}
    \end{definicion}
   
    \begin{proposicion}[Caracterización]
        \cite[p. ~118]{Renato}
        Los polinomios de Chalier $\charlier$ son la solución de la ecuación hipergeométrica para
        \begin{align}
            \sigma(x) &= x , \\
            \tau(x) &= \mu - x \\
            \lambda_n &= n, \quad \forall n \in \N_0 .
        \end{align}
    \end{proposicion}

    Con esta información procedemos a calcular los primeros términos de la sucesión $\charlier$.
    Primeramente, fijamos $C_{-1}^{(\mu)}(x) = 0$ y $C_0^{(\mu)}(x) = 1$.

    Los siguientes términos los podemos calcular mediante la relación de recurrencia a tres términos para SPOM, particularizada para el caso que nos ocupa:
    \begin{align}
        C_{n+1}^{(\mu)}(x) &= (x - \beta_n ) C_{n}^{(\mu)}(x) - \gamma_n C_{n-1}^{(\mu)}(x) = \\
        &= \left( x - \frac{\pescalar{x C_n^{(\mu)}}{C_n^{(\mu)}}_C}{\pescalar{C_n^{(\mu)}}{C_n^{(\mu)}}_C} \right) C_n^{(\mu)}(x) - \frac{\pescalar{x C_n^{(\mu)}}{C_{n-1}^{(\mu)}}_C}{ \pescalar{C_{n-1}^{(\mu)}}{C_{n-1}^{()\mu}}_C} C_{n-1}^{(\mu)}(x) = \\
        &= \left (x - \frac{\sum_{x \in \N_0} x C_n^{(\mu)}(x)^2 \frac{\mu^x}{x!}}{\sum_{x \in \N_0} C_n^{(\mu)}(x)^2 \frac{\mu^x}{x!}} \right ) C_{n}^{(\mu)}(x) - \frac{\sum_{x \in \N_0} x C_n^{(\mu)}(x) C_{n-1}^{(\mu)}(x) \frac{\mu^x}{x!}}{ \sum_{x \in \N_0} C_{n-1}^{(\mu)}(x)^2 \frac{\mu^x}{x!} } C_{n-1}^{(\mu)}(x) .
    \end{align}
    
    Pero en esta expresión aparecen cuatro sumas de series, a priori difíciles de calcular sin una teoría matemática auxiliar. Una expresión sencilla de esta relación de recurrencia la encontramos en la siguiente proposición.
    \begin{proposicion}[Recurrencia]
        \cite[p. ~170]{Chihara}
        Los polinomios de Charlier $\charlier$ cumplen la relación de recurrencia
        \begin{equation}\label{eq:recurrencia_charlier}
            C_{n+1}^{(\mu)}(x) = (x - n - \mu) C_n^{(\mu)}(x) - \mu n C_{n-1}^{(\mu)}(x), \quad \forall n \in \N.
        \end{equation}
    \end{proposicion}

    Así, calculamos fácilmente los primeros términos de los polinomios de Charlier:
    \begin{align}
        C_0^{(\mu)}(x) &= 1, \\
        C_1^{(\mu)}(x) &= x-\mu, \\
        C_2^{(\mu)}(x) &= x^2 + (-2\mu -1)x + \mu^2, \\
        C_3^{(\mu)}(x) &= x^3 - 3(\mu+1)x^2 + (3\mu^2+3\mu+2)x - \mu^3, \\
        \dots
    \end{align}

    En la siguiente proposición vemos una fórmula no recursiva para calcular los polinomios de Charlier mediante una suma finita.
    \begin{proposicion}[Fórmula explícita]
        El polinomio de Charlier de grado $n \in \N_0$ es
        \begin{equation}\label{eq:charlier}
            C_n^{(\mu)}(x) = \sum_{k=0}^n \binom{n}{k} (x-k+1)_{k} (-\mu)^{n-k} .
        \end{equation}
    \end{proposicion}
    \begin{proof}
        Demostramos que el polinomio \eqref{eq:charlier} es solución de la ecuación en diferencias \eqref{eq:recurrencia_charlier}.
        
        Para facilitar los cálculos, denotamos
        \begin{align}
            F_n^{(\mu)}(x) &= (x-n) C_n^{(\mu)}(x) , \\
            G_n^{(\mu)}(x) &= -\mu C_n^{(\mu)}(x) , \\
            H_n^{(\mu)}(x) &= -n \mu C_{n-1}^{(\mu)}(x).
        \end{align}

        Entonces, la ecuación \eqref{eq:recurrencia_charlier} queda como
        \begin{equation}
            C_{n+1}^{(\mu)}(x) = F_n^{(\mu)}(x) + G_n^{(\mu)}(x) + H_n^{(\mu)}(x) .
        \end{equation}

        Sustituimos en el miembro derecho de esta ecuación $C_n^{(\mu)}$ y $C_{n-1}^{(\mu)}$ por su valor según \eqref{eq:charlier}, lo operamos y obtenemos el miembro izquierdo. Para ellos, vamos a desarrollar primero $F_n^{(\mu)}$, $G_n^{(\mu)}$ y $H_n^{(\mu)}$ de forma separada.
        \begingroup
        \allowdisplaybreaks
        \begin{align}
            F_n^{(\mu)}(x) &= (x-n) C_n^{(\mu)}(x) \\
            &= (x-n) \sum_{k=0}^n \binom{n}{k} (x-k+1)_{k} (-\mu)^{n-k} \\
            &= \sum_{k=0}^n (x-n) \binom{n}{k} (x-k+1)_{k} (-\mu)^{n-k} \\
            &= \sum_{k=0}^n (x-k-(n-k)) \binom{n}{k} (x-k+1)_{k} (-\mu)^{n-k} \\
            &= \sum_{k=0}^n (x-k) \binom{n}{k} (x-k+1)_{k} (-\mu)^{n-k}
            - \sum_{k=0}^n (n-k) \binom{n}{k} (x-k+1)_{k} (-\mu)^{n-k} \\
            &= \sum_{k=1}^{n+1} (x-k+1) \binom{n}{k-1} (x-k+2)_{k-1} (-\mu)^{n-k+1}
            - \sum_{k=0}^{n-1} (n-k) \binom{n}{k} (x-k+1)_{k} (-\mu)^{n-k} \\
            &= \sum_{k=1}^{n+1} \binom{n}{k-1} (x-k+1)_{k} (-\mu)^{n-k+1}
            - \sum_{k=0}^{n-1} \frac{n!}{k!(n-k-1)!} (x-k+1)_{k} (-\mu)^{n-k} ; \\
            \\
            G_n^{(\mu)}(x) &= -\mu C_n^{(\mu)}(x) \\
            &= -\mu \sum_{k=0}^n \binom{n}{k} (x-k+1)_{k} (-\mu)^{n-k} \\
            &= \sum_{k=0}^n \binom{n}{k} (x-k+1)_{k} (-\mu)^{n-k+1} . \\
            \\
            H_n^{(\mu)}(x) &= -n \mu C_{n-1}^{(\mu)}(x) \\
            &= -n \mu \sum_{k=0}^{n-1} \binom{n-1}{k} (x-k+1)_{k} (-\mu)^{n-k-1} \\
            &= \sum_{k=0}^{n-1} \frac{n!}{k!(n-1-k)!} (x-k+1)_{k} (-\mu)^{n-k} .
        \end{align}
        \endgroup

        Entonces,
        \begin{align}
            F_n^{(\mu)}(x) + G_n^{(\mu)}(x) + H_n^{(\mu)}(x)
            &= \sum_{k=1}^{n+1} \binom{n}{k-1} (x-k+1)_{k} (-\mu)^{n-k+1}
            + \sum_{k=0}^n \binom{n}{k} (x-k+1)_{k} (-\mu)^{n-k+1} \\
            &= (x-n)_{n+1} + (-\mu)^{n+1}
            + \sum_{k=1}^n \left[ \binom{n}{k-1} + \binom{n}{k} \right](x-k+1)_{k} (-\mu)^{n-k+1} \\
            &= (x-n)_{n+1} + (-\mu)^{n+1}
            + \sum_{k=1}^n \binom{n+1}{k}(x-k+1)_{k} (-\mu)^{n-k+1} \\
            &= \sum_{k=0}^{n+1} \binom{n+1}{k}(x-k+1)_{k} (-\mu)^{n-k+1} \\
            &= C_{n+1}^{(\mu)}(x),
        \end{align}
        como queríamos demostrar.
    \end{proof}

    \begin{figure}[H]
        \centering
        \includegraphics[width=0.75\textwidth]{img/grafica_charlier.png}
        \caption{Representación gráfica de los primeros $4$ polinomios de Charlier para $\mu = 4$.}
    \end{figure}


    \subsection{Polinomios de Meixner}
    \begin{definicion}
        \cite{Filipuk}
        Los \emph{polinomios de Meixner} $\meixner$, de parámetros $\beta \in \R^+$ y $c \in ]0, 1[$, son la SPOVD con respecto al funcional de momentos
        \begin{equation}
            \funl[P] = \sum_{x \in \N_0} P(x) \frac{(\beta)_x c^x}{x!},  \quad \forall P \in \R[x].
        \end{equation}
        O, equivalentemente, con respecto al producto escalar
        \begin{equation}
            \pescalar{P}{Q}_M = \sum_{x \in \N_0} P(x) Q(x) \frac{(\beta)_x c^x}{x!}, \quad \forall P, Q \in \R[x] .
        \end{equation}
    \end{definicion}

    \begin{proposicion}[Caracterización]
        \cite[p. ~118]{Renato}
        Los polinomios de Meixner $\meixner$ son las soluciones de la ecuación hipergeométrica para
        \begin{align}
            \sigma(x) &= x \\
            \tau(x) &= (c - 1)x + c \beta \\
            \lambda_n &= (1-c)n, \quad \forall n \in \N_0 .
        \end{align}
    \end{proposicion}

    \begin{proposicion}[Recurrencia]
        \cite[p. ~176]{Chihara}
        Los polinomios de Meixner $\meixner$ cumplen la relación de recurrencia
        \begin{equation}
            c M_{n+1}^{(\beta, c)}(x) = [(c-1)x + (1+c)n + c \beta] M_n^{(\beta, c)}(x) - n (n+\beta-1) M_{n-1}^{(\beta, c)}(x), \quad \forall n \in \N.
        \end{equation}
    \end{proposicion}

    \begin{proposicion}[Fórmula explícita]
        \cite[p. ~176]{Chihara}
        El polinomio de Meixner de grado $n \in \N_0$ es
        \begin{equation}
            M_n^{(\beta, c)}(x) = (-1)^n n! \sum_{k=0}^n \binom{x}{k} \binom{-x-\beta}{n-k}c^{-k}.
        \end{equation}
    \end{proposicion}
    \begin{observacion}
        Esta fórmula tiene una utilidad teórica. En la práctica, para una máquina es complicada de utilizar, puesto que tal y como está escrita, tendría que calcular funciones $\Gamma$ dependientes del símbolo $x$. Si aplicamos la Observación \ref{ob:combinatorio} a los números combinatorios que aparecen obtenemos una fórmula más manejable:
        \begin{align}
            M_n^{(\beta, c)}(x) &= (-1)^n n! \sum_{k=0}^n \frac{(x-k+1)_k}{k!} \frac{(-x+\beta-n+k+1)_{n-k}}{(n-k)!} c^{-k} \\
            &= (-1)^n \sum_{k=0}^n \binom{n}{k} (x-k+1)_k (-x+\beta-n+k+1)_{n-k} c^{-k}.
        \end{align}
    \end{observacion}

    \begin{figure}[H]
        \centering
        \includegraphics[width=0.75\textwidth]{img/grafica_meixner.png}
        \caption{Representación gráfica de los primeros $6$ polinomios de Meiner para $\beta = 1, c = 0.5$.}
    \end{figure}

    \subsection{Polinomios de Krawtchouk}
    \begin{definicion}
        \cite[p. ~118]{Renato}
        Sea $N \in \N$. Los \emph{polinomios de Krawtchouk} $\kraw$, de parámetro $p \in ]0, 1[$, son la SPOVD con respecto al funcional de momentos
        \begin{equation}
            \funl[P] = \sum_{x=0}^{N} P(x) \binom{N}{x} p^x (1-p)^{N-x}, \quad \forall P \in \R[x].
        \end{equation}
        O, equivalentemente, con respecto al producto escalar
        \begin{equation}
            \pescalar{P}{Q}_K = \sum_{x=0}^{N} P(x)Q(x) \binom{N}{x} p^x (1-p)^{N-x}, \quad \forall P, Q \in \R[x].
        \end{equation}
    \end{definicion}

    \begin{proposicion}[Caracterización]
        \cite[p. ~118]{Renato}
        Los polinomios de Krawtchouk $\kraw$ son las soluciones de la ecuación hipergeométrica para
        \begin{align}
            \sigma(x) &= x \\
            \tau(x) &= \frac{Np - x}{1-p} \\
            \lambda_n &= \frac{n}{1-p}, \quad \forall n \in \{0, \dots, N-1\} .
        \end{align}
    \end{proposicion}

    \begin{proposicion}[Recurrencia]
        \cite[p. ~237]{Koekoek}
        Los polinomios de Krawtchouk $\kraw$ verifican la relación de recurrencia
        \begin{equation} \label{eq:Krawtchouk}
            p (N-n) K^{(p)}_{n+1}(x) = (-x + p(N-n) + n(1-p)) K^{(p)}_{n}(x) - n(1-p)K^{(p)}_{n-1}(x), \quad \forall n \in \{1, \dots, N-2\}.
        \end{equation}
    \end{proposicion}

    \begin{proposicion}[Fórmula explícita]
        \cite[p. ~237]{Koekoek}
        El polinomio de Krawtchouk de grado $n \in \{0, \dots, N-1\}$ es
        \begin{equation}
            K^{(p)}_n(x) = \sum_{k=0}^n \frac{(-n)_k (-x)_k}{(-N)_k} \frac{p^{-k}}{k!}.
        \end{equation}
    \end{proposicion}

    \begin{observacion}
        Los polinomios de Krawtchouk, por definición, son finitos. Esta propiedad está en consonancia con su recurrencia y fórmula explícita, que dejan de tener sentido si tomamos $n = N$.  
    \end{observacion}

    \begin{figure}[H]
        \centering
        \includegraphics[width=0.75\textwidth]{img/grafica_krawtchouk.png}
        \caption{Representación gráfica de los polinomios de Krawtchouk para $N=7, p=0.85$.}
    \end{figure}

    \subsection{Polinomios de Hahn}
    \begin{definicion}
        \cite[p. ~118]{Renato}
        Sea $N \in \N$. Los \emph{polinomios de Hahn} $\hahn$, de parámetros $\alpha, \beta \geq -1$, son la SPOVD con respecto al funcional de momentos
        \begin{equation}
            \funl[P] = \sum_{x=0}^{N} P(x) \frac{\Gamma(N+\alpha-x) \Gamma(\beta+x+1)}{\Gamma(N-x) \Gamma(x+1)}, \quad \forall P \in \R[x].
        \end{equation}
        O, equivalentemente, con respecto al producto escalar
        \begin{equation}
            \pescalar{P}{Q}_h = \sum_{x=0}^{N} P(x)Q(x) \frac{\Gamma(N+\alpha-x) \Gamma(\beta+x+1)}{\Gamma(N-x) \Gamma(x+1)}, \quad \forall P, Q \in \R[x].
        \end{equation}
    \end{definicion}

    \begin{proposicion}[Caracterización]
        \cite[p. ~118]{Renato}
        Los polinomios de Hahn $\hahn$ son las soluciones de la ecuación hipergeométrica para
        \begin{align}
            \sigma(x) &= x(N + \alpha - x) \\
            \tau(x) &= (\beta+1)(N-1)-(\alpha+\beta+2)x \\
            \lambda_n &= n(n+\alpha+\beta+1), \quad \forall n \in \{0, \dots, N-1\} .
        \end{align}
    \end{proposicion}

    \begin{proposicion}[Recurrencia] \cite[p. ~204]{Koekoek}
        Los polinomios de Hahn $\hahn$ cumplen la relación de recurrencia
        \begin{equation}
           -x h^{(\alpha, \beta)}_n(x) = A_n h^{(\alpha, \beta)}_{n+1}(x) - (A_n + C_n) h^{(\alpha, \beta)}_n(x) + C_n h^{(\alpha, \beta)}_{n-1}(x), \quad \forall n \in \{1, 2, \dots, N-2\},
        \end{equation}
        donde
        \begin{align}
            A_n &= \frac{(n+\alpha+\beta+1)(n+\alpha+1)(N-n)}{(2n+\alpha+\beta+1)(2n+\alpha+\beta+2)}, \\
            B_n &= \frac{n(n+\alpha+\beta+N+1)(n+\beta)}{(2n+\alpha+\beta)(2n+\alpha+\beta+1)}.
        \end{align}
    \end{proposicion}

    \begin{proposicion}[Fórmula explícita] \cite[p. ~204]{Koekoek}
        El polinomio de Hahn de grado $n \in \{0, 1, \dots, N-1\}$ es
        \begin{equation}
            h^{(\alpha, \beta)}_n(x) = \sum_{k=0}^n \frac{(-n)_k (n+\alpha+\beta+1)_k (-x)_k}{(\alpha+1)_k (-N)_k} \frac{1}{k!}.
        \end{equation}
    \end{proposicion}

    \begin{observacion}
        Los polinomios de Hahn, por definición, son finitos. Esta propiedad está en consonancia con su recurrencia y fórmula explícita, que dejan de tener sentido si tomamos $n = N$.
    \end{observacion}

    \begin{figure}[H]
        \centering
        \includegraphics[width=0.75\textwidth]{img/grafica_hahn.png}
        \caption{Representación gráfica de los primeros $7$ polinomios de Hahn para $N=25, \alpha=-0.5, \beta=1.5$.}
    \end{figure}
\endinput