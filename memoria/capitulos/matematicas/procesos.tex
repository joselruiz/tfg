% !TeX root = ../libro.tex
% !TeX encoding = utf8

%\setchapterpreamble[c][0.75\linewidth]{%
%	\sffamily
%  Breve resumen del capítulo. TODO
%	\par\bigskip
%}
\chapter{Procesos de Nacimiento y Muerte}\label{ch:tercer-capitulo}
El objetivo de este capítulo es poner de manifiesto la relación de los Procesos de Nacimiento y Muerte con los Polinomios Ortogonales. Para ello, primeramente nos centraremos en recordar conceptos básicos de Teoría de la Probabilidad, para después dar una definición rigurosa de Procesos de Nacimiento y Muerte, además de algunas propiedades. Todo ello nos permitirá exponer la susodicha conexión.

Las principales fuentes bibliográficas han sido \cite{Patrick}, \cite{Basicsto}, \cite{Wim} y \cite{Dominguez}.

\section{Conceptos básicos}\label{sec:3.1}
Esta sección es una lista de definiciones utilizadas posteriormente en el capítulo. Inicialmente, el lector puede obviarla y volver a ella con posterioridad si necesita recordar alguna definición.

\begin{definicion} \cite[p. ~22]{Patrick}
    Sea $\Omega$ un conjunto y $\mathcal F \subset \mathcal P (\Omega)$ \footnote{Denotamos por $\mathcal P(\Omega)$ al conjunto de las partes de $\Omega$, esto es, al conjunto de todos los subconjuntos de X.}. Una \emph{medida de probabilidad} en $\mathcal F$ es una aplicación $P: \mathcal P(\Omega) \longrightarrow \R$ satisfaciendo:
    \begin{itemize}
        \item $P(A) \in [0, 1]$, $\forall A \in \mathcal F$;
        \item $P(\emptyset) = 0$, $P(\Omega) = 1$;
        \item Si $\{A_n\}_{n \in \N}$ es una sucesión de elementos de $\mathcal F$, con $A_i \cap A_j = \emptyset$, $i \neq j$, y $\cup_{n \in \N} A_n \in \mathcal F$ entonces
              \begin{equation}
                  P( \cup_{n \in \N} A_n) = \sum_{n \in \N} P(A_n).
              \end{equation}
    \end{itemize}
\end{definicion}

\begin{definicion} \cite[p. ~160]{Patrick}
    Sea $\Omega$ un conjunto y y $\mathcal F \subset \mathcal P (\Omega)$. Una \emph{medida} en $\mathcal F$ es una aplicación $\mu: \mathcal P(\Omega) \longrightarrow \R \cup \{\infty\}$ tal que
    \begin{itemize}
        \item $\mu(A) \in [0, \infty]$, $\forall A \in \mathcal F$;
        \item $\mu(\emptyset) = 0$;
        \item Si $\{A_n\}_{n \in \N}$ es una sucesión de elementos de $\mathcal F$, con $A_i \cap A_j = \emptyset$, $i \neq j$, y $\cup_{n \in \N} A_n \in \mathcal F$ entonces
        \begin{equation}
            \mu( \cup_{n \in \N} A_n) = \sum_{n \in \N} \mu(A_n).
        \end{equation}
    \end{itemize}
\end{definicion}
\begin{observacion}
    Si se cumple que $\mu(\Omega) = 1$, entonces $\mu$ es una medida de probabilidad.
\end{observacion}

\begin{definicion} \cite[p. ~19]{Patrick}
    Sea $\Omega$ un conjunto. Una $\sigma$-álgebra en $\Omega$ es un conjunto $\mathcal A \in \mathcal P(\Omega)$ cumpliendo:
    \begin{itemize}
        \item $\emptyset \in \mathcal A$.
        \item Si $A \in \mathcal A$, entonces $\Omega - A \in \mathcal A$.
        \item Si $\{A_n\}_{n \in \N}$ es una sucesión de elementos de $\mathcal A$, entonces $\cup_{n \in \N} A_n \in \mathcal A$.
    \end{itemize}
\end{definicion}

\begin{definicion}
    \cite[p. ~23]{Patrick}
    Un \emph{espacio de probabilidad} es una terna $\eprob$, donde
    \begin{itemize}
        \item $\Omega$ es un conjunto,
        \item $\mathcal A$ es una $\sigma$-álgebra en $\Omega$ y
        \item $P: \mathcal A \longrightarrow [0, 1]$ es una medida de probabilidad.
    \end{itemize}
\end{definicion}

\begin{definicion}
    \cite[p. ~3]{Basicsto}
    Una \emph{variable aleatoria} sobre un espacio de probabilidad $\eprob$ es una aplicación $X: \Omega \longrightarrow \R$ tal que
    \begin{equation}
        X^{-1}(B) = \{ \omega \in \Omega: X(\omega) \in B \} \in \mathcal A, \quad \forall B \in \mathcal B. \footnote{$\mathcal B$ denota la $\sigma$-álgebra de Borel de $\R$.}
    \end{equation}
\end{definicion}

\begin{definicion}
    \cite[p. ~3]{Basicsto}

    Sea $X$ una variable aleatoria sobre un espacio de probabilidad $\eprob$.

    La \emph{distribución} de $X$ es la medida de probabilidad $P_X: \mathcal B \longrightarrow [0, 1]$,
    \begin{equation}
        P_X(B) = P(X^{-1}(B)), \quad \forall B \in \mathcal B.
    \end{equation}

    La \emph{función de distribución} de $X$ es la función $F_X: \R \longrightarrow [0,1]$,
    \begin{equation}
        F_X(x) = P_X(]- \infty, x]), \quad \forall x \in \R.
    \end{equation}
\end{definicion}
\begin{observacion}
    Utilizaremos indistintamente las siguientes notaciones:
    \begin{equation}
        P_X(]- \infty, x]) =  P_X(X \leq x) = P(X \leq x).
    \end{equation}
\end{observacion}

\begin{definicion}
    \cite[p. ~139]{Basicsto}, \cite[p. ~15]{Wim}

    Sea $T \subset \R$ un conjunto arbitrario. Un \emph{proceso estocástico} es un conjunto de variables aleatorias $\pest$, sobre un espacio de probabilidad $\eprob$.

    Si $T = \N$, lo llamamos \emph{proceso estocástico en tiempo discreto}.

    Si $T$ es un intervalo de $\R$, típicamente $T = [0, \infty[$, decimos que es un \emph{proceso estocástico en tiempo continuo}.

    Al conjunto $S = \{X_t(\omega): \omega \in \Omega, t \in T\}$ lo llamamos \emph{espacio de estados} y cada uno de los elementos, \emph{estado}.
\end{definicion}

\begin{definicion}
    \cite[p. ~8]{Basicsto}
    Sean $X$, $Y$ dos variables aleatorias. Definimos la probabilidad de $X=x$ condicionada a que $Y=y$ como\footnote{Normalmente esta propiedad se enuncia llamando a $X=x$ suceso A y a $Y=y$, suceso B, quedando $P(A \vert B) = \frac{P(A \cap B)}{P(B)}$, aunque en nuestro caso será más útil la notación dada para expresar la propiedad Markoviana.}
    \begin{equation}
        P(X=x \vert Y=y) = \frac{P(X=x, Y=y)}{P(Y=y)}.
    \end{equation} 
\end{definicion}

\section{Cadenas de Markov y Procesos de Nacimiento y Muerte}\label{sec:3.2}
\begin{definicion}
    \cite[p. ~58]{Dominguez}
    Un \emph{proceso de Markov en tiempo discreto} o \emph{cadena de Markov} es un proceso estocástico  $\pest$, con $T$ conjunto numerable, que verifica la \emph{propiedad de Markov}, es decir,
    \begin{equation}
        P(X_{n+1} = j \vert X_0=i_0, X_1=i_1, \dots, X_{n-1} = i_{n-1}, X_n=i) = P(X_{n+1} \vert X_n=i),
    \end{equation}
    para cualesquiera $i_0, i_1, \dots, i_{n-1}, i, j$ en el espacio de estados $S$.
\end{definicion}
\begin{observacion}\cite[p. ~15]{Wim}
    Interpretando $n$ como el instante de tiempo actual, la propiedad de Markov establece que el comportamiento del proceso de Markov en el futuro, condicionado a su estado actual y pasado, depende únicamente del estado actual.
    Dicho de otro modo, $X_n$ contiene toda la información sobre la evolución pasada del proceso que es necesaria para determinar su comportamiento futuro.
\end{observacion}
\begin{observacion}
    En este capítulo consideraremos siempre $T = S = \N_0$.
\end{observacion}

\begin{definicion}
    \cite[p. ~147]{Dominguez}
    Sea $\markov$ una cadena de Markov. Para cada $(i, j) \in S^2 = \N_0^2$ se define la \emph{función de probabilidad de transición del estado $i$ al estado $j$} por:
    \begin{equation} \label{eq:ftransicion}
        \begin{split}
            P_{i, j} &: \N_0^2 \longrightarrow [0, 1] \\
            & P_{i, j}(s, t) = P(X_{s + t} = j | X_t = i), \quad \forall s, t \in T = \N_0.
        \end{split}
    \end{equation}
\end{definicion}

\begin{observacion}
    En la ecuación \eqref{eq:ftransicion} podemos tomar $s=1$ y $t=n \in \N_0$ y obtenemos
    \begin{equation}
        P_{i, j}(1, n) = P_{i, j}(n) = P(X_{n+1} = j \vert X_n = i), \quad  \forall i, j \in S = \N_0.
    \end{equation}
    Asumiremos siempre que la cadena de Markov es \emph{homogénea} o \emph{estacionaria en tiempo} \cite[p. ~58]{Dominguez}, lo que quiere decir que esta última probabilidad no depende del tiempo $n$, luego podemos escribir:
    \begin{equation}
        \boxed{P_{i,j} = P(X_{n+1} = j \vert X_n = i)}
    \end{equation}
\end{observacion}

\begin{definicion}\cite[p. ~58]{Dominguez}
    Los valores $P_{i,j}$ son las \emph{probabilidades de transición en un paso} y pueden ser representados por la \emph{matriz de probabilidades de transición en un paso} (infinita)
    \begin{equation}
        P = \left[ P_{i,j} \right]_{i, j \in \N_0} =
        \left[
            \begin{array}{ccccc}
                P_{0, 0} & P_{0, 1} & P_{0, 2} & P_{0, 3} & \dots  \\
                P_{1, 0} & P_{1, 1} & P_{1, 2} & P_{1, 3} & \dots  \\
                P_{2, 0} & P_{2, 1} & P_{2, 2} & P_{2, 3} & \dots  \\
                P_{3, 0} & P_{3, 1} & P_{3, 2} & P_{3, 3} & \dots  \\
                \vdots   & \vdots   & \vdots   & \vdots   & \ddots
            \end{array}
            \right].
    \end{equation}
\end{definicion}
\begin{observacion}
    $P$ es una \emph{matriz estocástica} (por filas), es decir, satisface
    \begin{equation}
        P_{i,j} > 0, \ i, j \in \N_0, \quad \quad \sum_{j \in \N_0}P_{i,j} = 1, \ \forall i \in \N_0.
    \end{equation}
\end{observacion}

\begin{observacion}
    Si en la función \eqref{eq:ftransicion} tomamos $s = n$ y $t=0$ obtenemos una función de una variable:
    \begin{equation}
        \begin{split}
            P_{i, j} &: \N_0 \longrightarrow [0, 1] \\
            & P_{i, j}(n) = P(X_n = j | X_0 = i) \ , \quad \forall n \in T = \N_0.
        \end{split}
    \end{equation}
    Para cada $n \in \N_0$, nos resultará más cómodo denotar $P_{i, j}^{(n)}$ a $P_{i, j}(n)$, por tanto:
    \begin{equation}
        \boxed{P_{i, j}^{(n)} = P(X_n = j | X_0 = i)}
    \end{equation}
\end{observacion}
\begin{definicion}
    Los valores $P_{i, j}^{(n)}$ son las \emph{probabilidades de transición en $n$ pasos} y pueden representarse en la \emph{matriz de probabilidades de transición en $n$ pasos}
    \begin{equation}
        P^{(n)} = \left[ P_{i,j}^{(n)} \right]_{i, j \in \N_0} =
        \left[
            \begin{array}{ccccc}
                P_{0, 0}^{(n)} & P_{0, 1}^{(n)} & P_{0, 2}^{(n)} & P_{0, 3}^{(n)} & \dots  \\
                P_{1, 0}^{(n)} & P_{1, 1}^{(n)} & P_{1, 2}^{(n)} & P_{1, 3}^{(n)} & \dots  \\
                P_{2, 0}^{(n)} & P_{2, 1}^{(n)} & P_{2, 2}^{(n)} & P_{2, 3}^{(n)} & \dots  \\
                P_{3, 0}^{(n)} & P_{3, 1}^{(n)} & P_{3, 2}^{(n)} & P_{3, 3}^{(n)} & \dots  \\
                \vdots         & \vdots         & \vdots         & \vdots         & \ddots
            \end{array}
            \right].
    \end{equation}
\end{definicion}

\begin{proposicion} \cite[p. ~58]{Dominguez}
    Con las suposiciones hechas, una cadena de Markov verifica:
    \begin{enumerate}
        \item $P^{(0)} = I$, donde $I$ denota la matriz identidad infinita.
        \item $P^{(1)} = P$.
        \item $P^{(n+1)} = P^{(n)} P = P P^{(n)}$, $\forall n \in \N_0$.
    \end{enumerate}
\end{proposicion}

La última relación puede generalizarse en la siguiente proposición.

\begin{proposicion}[Ecuaciones de Chapman-Kolmogorov]
    \cite[p. ~59]{Dominguez}

    Una cadena de Markov cumple
    \begin{equation}
        P_{i, j}^{(n)} = \sum_{k \in \N_0} P_{i, k}^{(k)} P_{k, j}^{(n-k)}, \quad \forall n \in \N_0.
    \end{equation}
\end{proposicion}


\begin{definicion}\cite[p. ~59]{Dominguez}
    Sean $i, j \in \N_0$ estados de una cadena de Markov. Decimos que
    \begin{itemize}
        \item \emph{$j$ es accesible desde $i$} si existe algún $n\in \N_0$ tal que $P_{i, j}^{(n)} > 0$;
        \item \emph{$i$ y $j$ están comunicados} si $j$ es accesible desde $i$ e $i$ es accesible desde $j$;
        \item la cadena de Markov es \emph{irreducible} si todos los pares de estados están comunicados.
    \end{itemize}
\end{definicion}

\begin{definicion}\cite[p. ~59]{Dominguez}
    El \emph{periodo de un estado} $i$ de una cadena de Markov es
    \begin{equation}
        d(i) =
        \begin{cases}
            0                                           & \text{ si } P_{i, i}^{(n)} = 0, \ \forall n \in \N, \\
            \text{mcd}(\{n\in \N: P_{i, i}^{(n)} > 0\}) & \text{ en otro caso.}
        \end{cases}
    \end{equation}
\end{definicion}

\begin{definicion}
    Una cadena de Markov es aperiódica si $d(i) = 1$, para todo estado $i \in \N_0$.
\end{definicion}

\begin{definicion} \cite[p. ~59]{Dominguez}
    Sea $\markov$ una cadena de Markov. Para cada par de estados $i, j$ se define la \emph{función de probabilidad de primera transición del estado i al estado j} por:
    \begin{equation}
        \begin{split}
            f_{i, j} &: \N_0^2 \longrightarrow [0, 1] \\
            & f_{i, j}(n) = f_{i,j}^{(n)} = P(X_1 \neq i, X_2 \neq i, \dots, X_{n-1}\neq i, X_n = j \vert X_0 = i), \quad \forall n \in T = \N.
        \end{split}
    \end{equation}
\end{definicion}

\begin{comment}
\begin{definicion}\cite[p. ~60]{Dominguez}
    \begin{enumerate}
        \item Estado recurrente etc
    \end{enumerate}
\end{definicion}

TODO: incluir algunas definciones.  NUEVO : parece que no son necesarias para después.
\end{comment}


\begin{definicion}\label{def:pnm}
    \cite[p. ~62]{Dominguez}
    Un \emph{proceso de nacimiento y muerte en tiempo discreto} o \emph{cadena de nacimiento y muerte}, \emph{PNM}, es una cadena de Markov $\markov$ con espacio de estados $S = \N_0$, cuya matriz de probabilidades de transición en un paso  $P = [ P_{i, j} ]_{i, j \in \N_0}$ cumpliendo que
    \begin{equation}
        P_{i, j} =
        \begin{cases}
            p_i & \text{ si } j=i+1,                 \\
            r_i & \text{ si } j=i,                   \\
            q_i & \text{ si } j=i-1,                 \\
            0   & \text{ si } \vert i - j \vert > 1,
        \end{cases}
    \end{equation}
    para ciertas sucesiones de números reales $\{p_i\}_{i \in \N_0}$,  $\{r_i\}_{i \in \N_0}$ y  $\{q_i\}_{i \in \N}$ que hagan de $P$ una matriz estocástica.
\end{definicion}
\begin{observacion}
    Explícitamente, $P$ es una matriz tridiagonal de la forma
    \begin{equation}
        P =
        \left[
            \begin{array}{cccccc}
                r_0    & p_0    & 0      & 0      & 0      & \dots  \\
                q_1    & r_1    & p_1    & 0      & 0      & \dots  \\
                0      & q_2    & r_2    & p_2    & 0      & \dots  \\
                0      & 0      & q_3    & r_3    & p_3    & \dots  \\
                \vdots & \vdots & \vdots & \ddots & \ddots & \ddots
            \end{array}
            \right].
    \end{equation}
\end{observacion}

\begin{definicion} \cite[p. ~63]{Dominguez}
    Los \emph{coeficientes potenciales} de un PNM son los valores $\{\pi_n\}_{n \in \N_0}$ dados por
    \begin{equation}
        \pi_0 = 1, \quad \pi_n = \frac{p_0 p_1 \cdots p_{n-1}}{q_1 q_2 \cdots q_n}, \ \forall n \in \N.
    \end{equation}
\end{definicion}

\section{Polinomios de Nacimiento y Muerte}\label{sec:3.3}
Comenzamos la sección con la definición central del capítulo.
\begin{definicion}
    \cite[p. ~63]{Dominguez}
    Sea $P$ la matriz de probabilidades de transición en un paso de un PNM, como en la Definición \ref{def:pnm}. Se define la \emph{sucesión de polinomios de nacimiento y muerte (SPNM)} asociada a tal PNM como la sucesión de polinomios $\{Q_n\}_{n \geq -1}$ dada por:
    \begin{align} \label{eq:spnm}
        Q_{-1}(x) & = 0, \quad Q_0(x) = 1,                                                                  \\
        x Q_n(x)  & = p_n Q_{n+1}(x) + r_n Q_n(x) + q_n Q_{n-1}(x), \ \forall n \in \N_0.
    \end{align}
\end{definicion}
\begin{observacion}
    Por el Teorema de Favard, una SPNM es una SPO con respecto a algún funcional de momentos.
\end{observacion}
\begin{observacion}
    La relación de recurrencia a tres términos \eqref{eq:spnm} puede expresarse matricialmente como
    \begin{equation}
        x \mathcal Q = P \mathcal Q,
    \end{equation}
    donde $\mathcal Q = [Q_0(x), Q_1(x), Q_2(x), \dots]^T$ es un vector infinito.

    Observemos entonces que $P$ es una matriz de Jacobi, según la Definición \ref{def:matriz_jacobi}.
\end{observacion}

\begin{comment}
El siguiente teorema, el \autoref{th:unica_positiva_regular}, nos habla sobre la unicidad del funcional con respecto al cual la SPNM es una SPO. Para entenderlo, primero introducimos brevemente el concepto de distribución. TODO
\begin{teorema} \label{th:unica_positiva_regular}
    \cite[p. ~67]{Dominguez}
    Sea $\markov$ un PNM con matriz de probabilidades de transición en un paso $P$. Entonces existe una única distribución positiva regular $\psi$ con soporte en el intervalo $[-1, 1]$ verificando
    \begin{equation}
        P_{i, j}^{(n)} = \pi_j \int_{-1}^1 x^n Q_i(x)Q_j(x) d \psi(x), \quad \forall i, j, n \in \N_0.
    \end{equation}
\end{teorema}
\end{comment}

El siguiente teorema nos habla de una sucesión de polinomios ortogonales con respecto a una cierta medida $\psi$, de soporte el intervalo $[a,b]$. Esta denominación no es más que otra forma de decir que cada par de polinomios de la sucesión, $f$ y $g$, son ortogonales con respecto al producto escalar
\begin{equation}
    \pescalar{f}{g} = \int_a^b f(x) g(x) d\psi(x).
\end{equation}

\begin{teorema}
    \cite[p. ~68]{Dominguez}
    Sea $\psi$ una medida con soporte el intervalo $[-1, 1]$ y sea la sucesión de polinomios ortogonales mónicos $\{\widehat{P}_n\}_{n \geq -1}$ que satisface la relación de recurrencia a tres términos
    \begin{align} \label{eq:relacion_p_gorro}
        \widehat{P}_{-1}(x)  &= 0, \quad \widehat{P}_0(x)     = 1,                                                                              \\
        \widehat{P}_{n+1}(x) & = (x-\beta_n)\widehat{P}_n(x) - \gamma_n \widehat{P}_{n-1}(x), \ \forall n \in \N_0,
    \end{align}
    con $\beta_n \geq 0$, $\gamma_{n+1} >0$, $\forall n \in \N_0$. Definimos la sucesión $\{Q_n\}_{n \in \N_0}$ por
    \begin{equation}
        Q_n(x) = \frac{\widehat{P}_n(x)}{\widehat{P}_n(1)}, \quad \forall n \in \N_0.
    \end{equation}

    Entonces $\{\widehat{P}_n\}_{n \in \N_0}$ es una SPNM si y sólo si $\{Q_n\}_{n \in \N_0}$ lo es y los coeficientes de $\{Q_n\}_{n \in \N_0}$ en la relación \eqref{eq:spnm} cumplen
    \begin{equation} \label{eq:condicion_q}
        q_0 = 0, \quad p_n, q_{n+1} > 0, \quad r_n \geq 0, \quad \forall n \in \N_0.
    \end{equation}
\end{teorema}
\begin{proof}
    \boxed{\Rightarrow} Supongamos que $\{\widehat{P}_n\}_{n \in \N_0}$ es una SPNM. Como es una SPO con respecto a $\psi$, las raíces de $\widehat{P}_n$ se encuentran en el intervalo $[-1, 1]$, $\forall n \in \N$. Por ser una sucesión mónica, el coeficiente líder de $P_n$ es positivo y, al no tener raíces mayores que $1$, debe ser $\widehat{P}_n(1) > 0$, $\forall n \in \N_0$, luego la sucesión $\{Q_n\}_{n \in \N_0}$ está bien definida.

    Usamos que $\widehat{P}_n(x) = Q_n(x)\widehat{P}_n(1)$, $\forall n \in \N_0$, en la relación de recurrencia \eqref{eq:relacion_p_gorro}:
    \begin{align}
        Q_{-1}(x) &= 0, \quad Q_0(x)\widehat{P}_0(1) =1 \\
        Q_{n+1}(x)\widehat{P}_{n+1}(1) & = (x-\beta_n)Q_n(x)\widehat{P}_n(1) - \gamma_n Q_{n-1}(x)\widehat{P}_{n-1}(1), \ \forall n \in \N_0;
    \end{align}
    y operando,
    \begin{equation}
        x Q_n(x) = \frac{\widehat{P}_{n+1}(1)}{\widehat{P}_n(1)} Q_{n+1}(x) + \beta_n Q_n(x) + \gamma_n \frac{\widehat{P}_{n-1}(1)}{\widehat{P}_{n}(1)} Q_{n-1}(x), \forall n \in \N_0.
    \end{equation}

    Tomando $p_n = \frac{\widehat{P}_{n+1}(1)}{\widehat{P}_n(1)}$, $r_n = \beta_n$ y $\gamma_n \frac{\widehat{P}_{n-1}(1)}{\widehat{P}_{n}(1)}$, llegamos a que $\{Q_n\}_{n \in \N}$ es una SPNM y, además, satisface \eqref{eq:condicion_q}.

    \boxed{\Leftarrow} Consideramos que $\{Q_n\}_{n \in \N_0}$ es una SPNM y verifica \eqref{eq:condicion_q}. La relación \eqref{eq:spnm} de $\{Q_n\}_{n \in \N_0}$ puede reescribirse en términos de $\{\widehat{P}_n\}_{n \in \N_0}$, quedando
    \begin{equation}
        x \widehat{P}_n(x) = \frac{p_n \widehat{P}_n(1)}{\widehat{P}_{n+1}(1)} \widehat{P}_{n+1}(x) + r_n \widehat{P}_n + \frac{q_n \widehat{P}_n(1)}{\widehat{P}_{n-1}(1)} \widehat{P}_{n-1}(x),
    \end{equation}
    lo que nos dice que $\{\widehat P_n\}_{n \in \N_0}$ es también un SPNM.
\end{proof}

\begin{teorema}
    \cite[p. ~69]{Dominguez}
    Sea $\markov$ un PNM con matriz de probabilidades de transición $P$, donde $q_0 = 0$, y se considera la sucesión de polinomios $\{Q_n\}_{n \in \N_0}$ generada por la relación de recurrencia a tres términos \eqref{eq:spnm}. Entonces, para cualquier parámetro $\mu \in ]0, 1[$ existe una sucesión $\{a_n\}_{n \in \N_0}$, con $a_n > 0$, $\forall n \in \N_0$, tal que $R_n(x) = a_n Q_n(x)$ satisface la relación de recurrencia
    \begin{align} \label{eq:relacion_teorema_serie}
        R_0(x)   & = 1,                                                     \\
        x R_0(x) & = p_0'R_1(x) + (1-p_0' - \mu)R_0(x),                     \\
        x R_n(x) & = p_n'R_{n+1}(x) + (1-p_n'-q_n')R_n(x) + q_n'R_{n-1}(x),
    \end{align}
    con $p_n', q_n' \in ]0, 1[$, si y sólo si la serie $\sum_{n \in \N_0}(p_n \pi_n)^{-1}$ es convergente y $\mu$ satisface
    \begin{equation}
        \mu \sum_{n \in \N_0}\frac{1}{p_n \pi_n} < 1 .
    \end{equation}
\end{teorema}
\begin{proof}
    Sea $R_n(x) = a_n Q_n(x)$, $\forall n \in \N_0$. Esta nueva sucesión de polinomios satisface la relación de recurrencia
    \begin{align}
        R_0(x)   & = a_0,                                                                                          \\
        xR_0(x)  & = (1-p_0) R_0(x) + p_0 \frac{a_0}{a_1} R_1(x),                                                  \\
        x R_n(x) & = p_n \frac{a_n}{a_{n+1}}R_{n+1}(x)  + (1 - p_n - q_n)R_n(x) + q_n \frac{a_n}{a_{n-1}} R_{n-1}(x),
    \end{align}
    considerando $p_0+r_0 = 1$ y $p_n+r_n+q_n = 1$, $\forall n \in N_0$.
    
    Esta relación es del tipo \eqref{eq:relacion_teorema_serie} si y sólo si
    \begin{align}
        a_0                                               & = 1,         \\
        \mu + p_0 \frac{a_0}{a_1}                         & = p_0,       \\
        p_n \frac{a_n}{a_{n+1}} + q_n \frac{a_n}{a_{n-1}} & = p_n + q_n.
    \end{align}

    Sean las sucesiones de números reales dadas por
    \begin{align}
        \rho_0   & = \frac{\mu}{p_0}, \quad \rho_n = \frac{q_n}{p_n}, \quad  \sigma_n = \frac{a_n}{a_{n-1}}, \\
        \tau_n  & = \rho_0 + \rho_0 \rho_1 + \dots + (\rho_0 \rho_1 \cdots \rho_n) = \mu \sum_{k=0}^n \frac{1}{p_k \pi_k}, \quad \forall n \in \N_0.
    \end{align}

    Las relaciones anteriores podemos escribirlas como
    \begin{align}
        \rho_0 + \frac{1}{\sigma_1}              & = 1,          \\
        \rho_n \sigma_n + \frac{1}{\sigma_{n+1}} & = 1 + \rho_n.
    \end{align}

    Por inducción se prueba que $\sigma_n > 1$, $\forall n \in \N$.

    Sustituyendo $\sigma_1$ en $\sigma_2$ y así sucesivamente, tenemos
    \begin{equation}
        \sigma_{n+1} = 1 + \frac{\rho_0 \rho_1 \cdots \rho_n}{1 - \tau_n}.
    \end{equation}
    Entonces la ecuación tiene solución con $\sigma_n > 1$, $\forall n \in \N$, si y sólo si $\tau_n < 1$, $\forall n \in \N_0$.

    Los valores de $\sigma_{n+1}$ vienen dados por
    \begin{equation}
        \sigma_{n+1} = \frac{1 - \mu \sum_{k=0}^{n-1}\frac{1}{p_k \pi_k}}{1 - \mu \sum_{k=0}^n \frac{1}{p_k \pi_k}} ,
    \end{equation}
    y de aquí podemos calcular $a_n = \sigma_1 \sigma_2 \cdots \sigma_n$.
\end{proof}




\section{Ejemplo de Procesos de Nacimiento y Muerte}\label{sec:3.4}
En esta sección exponemos un ejemplo sencillo de PNM en tiempo discreto y con espacio de estados finito, y su relación con cierta familia de polinomios ortogonales. Para ello, nos basamos en \cite[p. ~88]{Dominguez}.

\subsection{Modelo de Ehrenfest del intercambio de calor entre dos cuerpos aislados}
Supongamos que tenemos un conjunto de $2N$ bolas, digamos $\{b_1, b_2, b_3, \dots, b_{2N}\}$; y dos urnas, $A$ y $B$. Inicialmente, en la urna $A$ introducimos $j$ bolas y en la $B$, las $2N-j$ bolas restantes.

En cada paso, elegimos al azar un número $k \in \{1, 2, \dots, 2N\}$ y consideramos la bola $b_k$. Si la bola $b_k$ está en la urna $A$ la cambiamos a la urna $B$ y si está en la $B$, la cambiamos a la $A$.

Para $n \in \N_0$, definimos la variable aleatoria $X_n$ como la que cuenta el número de bolas en la urna $A$ en el instante de tiempo $n$. Así, podemos definir el proceso estocástico en tiempo discreto $\{X_n\}_{n \in \N_0}$, cuyo espacio de estados es $S = \{0, 1, 2, \dots, 2N \}$. Evidentemente, la distribución de probabilidad de $X_n$ depende únicamente de $X_{n-1}$, $\forall n \in \N$, luego $\{X_n\}_{n \in \N_0}$ es una cadena de Markov.

Supongamos que en un cierto instante de tiempo $n \in \N_0$ la urna $A$ tiene $j \in S$ bolas, es decir, $X_n = j$. Entonces:
\begin{itemize}
    \item La probabilidad de que $A$ en el instante $n+1$ tenga también $j$ bolas es
          \begin{equation}
              r_j := P_{j, j} = P(X_{n+1} = j \vert X_n = j) = 0,
          \end{equation}
          porque en cada instante debe producirse el cambio de urna de alguna bola.

    \item La probabilidad de que $A$ en el instante $n+1$ gane una bola es
          \begin{equation}
              p_j := P_{j, j+1} = P(X_{n+1} = j+1 \vert X_n = j) = \frac{2N-j}{2N}.
          \end{equation}
          Claramente, $p_0 = 1$, porque si $A$ no tiene ninguna bola es seguro que en el instante siguiente tendrá exactamente $1$; y $p_{2N} = 0$, porque si $A$ contiene todas las bolas no puede adquirir ninguna más.

    \item La probabilidad de que $A$ en el instante $n+1$ pierda una bola es
          \begin{equation}
              q_j := P_{j, j-1} = P(X_{n+1} = j-1 \vert X_n = j) = \frac{j}{2N}.
          \end{equation}
          Claramente, $q_{0} = 0$, porque si $A$ no tiene ninguna bola no puede perderla; y $q_{2N} = 1$, porque si $A$ tiene todas las bolas es seguro que en siguiente instante tendrá una menos.

    \item La probabilidad de que $A$ en el instante de $n+1$ tenga $i$ bolas, con $i < j-1$ ó $i > j - 1$ es
          \begin{equation}
              P_{j, i} = P(X_{n+1} = i \vert X_n = j) = 0,
          \end{equation}
          porque $A$ no puede ganar o perder más de una bola en cada paso.
\end{itemize}

Podemos agrupar estas probabilidades en una matriz de probabilidades de transición en un paso:
\begin{equation}
    P = \left[
        \begin{array}{ccccccc}
            r_0 & p_0 & 0      & 0        & 0        & 0        \\
            q_1 & r_1 & p_1    & 0        & 0        & 0        \\
            0   & q_2 & r_2    & p_2      & 0        & 0        \\
            0   & 0   & \ddots & \ddots   & \ddots   & 0        \\
            0   & 0   & 0      & q_{2N-1} & r_{2N-1} & p_{2N-1} \\
            0   & 0   & 0      & 0        & q_{2N}   & r_{2N}
        \end{array}
        \right]
    = \left[
        \begin{array}{ccccccc}
            0            & 1            & 0               & 0               & 0      & 0            \\
            \frac{1}{2N} & 0            & \frac{2N-1}{2N} & 0               & 0      & 0            \\
            0            & \frac{2}{2N} & 0               & \frac{2N-2}{2N} & 0      & 0            \\
            0            & 0            & \ddots          & \ddots          & \ddots & 0            \\
            0            & 0            & 0               & \frac{2N-1}{2N} & 0      & \frac{1}{2N} \\
            0            & 0            & 0               & 0               & 1      & 0
        \end{array}
        \right],
\end{equation}
y vemos que, en efecto, $\{X_n\}_{n \in \N_0}$ es un proceso de nacimiento y muerte.

Con la Figura \ref{fig:ehrenfest} expresamos gráficamente la esencia del proceso. Cada círculo representa un estado y las flechas, las probabilidades de pasar a los estados adyacentes.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\textwidth]{img/ehrenfest.jpg}
    \caption{Diagrama de transiciones en un paso del modelo de Ehrenfest}
    \label{fig:ehrenfest}
\end{figure}

Consideremos la relación a recurrencia a tres términos de los polinomios de Krawtchouk, la ecuación \eqref{eq:Krawtchouk}. Si tomamos $2N$ en lugar de $N$, llamamos $j$ al subíndice y escogemos el parámetro $p=1/2$, la relación puede escribirse
\begin{equation}
    \frac{2N-j}{2} K_{j+1}(x) = (-x+N) K_j(x) - \frac{j}{2}K_{j-1}(x).
\end{equation}
Dividiendo por $N$ y pasando a la izquierda el último sumando:
\begin{equation} \label{eq:ehrenfest_reccurrencia}
    \frac{2N-j}{2N} K_{j+1}(x) + \frac{j}{2N}K_{j-1}(x) = \left(1-\frac{x}{N}\right) K_j(x).
\end{equation}

Sea $\mathcal K(x) = [K_0(x), K_1(x), K_2(x), \dots, K_{2N}(x)]^T$ el vector formado por los polinomios de Krawtchouk, que recordemos que son finitos. Entonces, la relación \eqref{eq:ehrenfest_reccurrencia} puede expresarse matricialemente:
\begin{equation}
    P \mathcal K(x) = \left(1 - \frac{x}{N}\right) \mathcal K(x),
\end{equation}
donde $P$ denota la matriz de probabilidades de transición de este proceso, anteriormente calculada.

%TODO: Por tanto, los valores propios de $P$ son el conjunto $\{1-\frac{x}{N}: x \in \R\}$. En el libro dice que la x es natural ¿?
\begin{comment}
\subsection{Modelo de urnas de Bernoulli-Laplace}
Consideremos $\alpha$ bolas blancas y $\beta$ bolas negras, con $\beta \geq \alpha$ y dos urnas, $A$ y $B$. Repartimos las $\alpha + \beta$ bolas entre ambas urnas, de forma que en $A$ haya exactamente $\alpha$ bolas y en $B$, las $\beta$ bolas restantes\footnote{Notemos que esto no quiere decir que $A$ tenga todas las bolas blancas o $B$ todas las negras, sino que, entre blancas y negras, $A$ tiene $\alpha$ bolas y $B$, $\beta$ bolas.}.

En cada paso, elegimos al azar una bola de cada urna y las intercambiamos, manteniendo constante el número de bolas que contiene cada urna.

Para cada $n \in \N_0$, definimos la variable aleatoria $X_n$ como la que cuenta las bolas blancas que hay en $A$ en el instante de tiempo $n$. De esta forma podemos definir un proceso estocástico en tiempo discreto, $\{X_n\}_{n \in \N_0}$, con espacio de estados $S = \{0, 1, 2, \dots, \alpha\}$.

Supongamos que un cierto instante de tiempo $n \in \N_0$ la urna $A$ tiene $j \in S$ bolas blancas y $\alpha-j$ bolas negras. Entonces, $B$ tiene $\alpha-j$ bolas blancas y $\beta-\alpha+j$ bolas negras. Esto lo podemos expresar como $X_n = j$. En estas condiciones:
\begin{itemize}
    \item La probabilidad de que $A$ en el instante $n+1$ tenga también $j$ bolas blancas es
          \begin{equation}
              r_j := P_{j,j} = P(X_{n+1} = j \vert X_n = j) = \frac{j}{\alpha}\frac{\alpha-j}{\beta} + \frac{\alpha-j}{\alpha} \frac{\beta-\alpha+j}{\beta}.
          \end{equation}

    \item La probabilidad de que $A$ en el instante $n+1$ tenga $j+1$ bolas blancas es
          \begin{equation}
              p_j := P_{j, j+1} = P(X_{n+1} = j+1 \vert X_n = j) = \frac{\alpha-j}{\alpha} \frac{\alpha-j}{\beta}.
          \end{equation}

    \item La probabilidad de que $A$ en el instante $n+1$ tenga $j-1$ bolas blancas es
          \begin{equation}
              q_j := P_{j, j-1} = P(X_{n+1} = j-1 \vert X_n = j) = \frac{j}{\alpha} \frac{\beta-\alpha+j}{\beta}.
          \end{equation}

    \item La probabiladad de que $A$ en el instante $n+1$ tenga $i$ bolas blancas, con $i < j-1$ ó $i > j+1$ es
          \begin{equation}
              P_{j, i} = P(X_{n+1} = i \vert X_n = j) = 0,
          \end{equation}
          porque no $A$ no puede ganar o perder más de una bola en un paso.
\end{itemize}

Agrupamos estas probabilidades en una matriz de probabilidades de transición en un paso:
\begin{align}
    P & = \left[
        \begin{array}{ccccccc}
            r_0 & p_0 & 0      & 0            & 0            & 0            \\
            q_1 & r_1 & p_1    & 0            & 0            & 0            \\
            0   & q_2 & r_2    & p_2          & 0            & 0            \\
            0   & 0   & \ddots & \ddots       & \ddots       & 0            \\
            0   & 0   & 0      & q_{\alpha-1} & r_{\alpha-1} & p_{\alpha-1} \\
            0   & 0   & 0      & 0            & q_{\alpha}   & r_{\alpha}
        \end{array}
        \right]  \\
      & = \left[
        \begin{array}{ccccccc}
            \frac{\beta-\alpha}{\beta}          & \frac{\alpha}{\beta}                            & 0                                              & 0                                        & 0                                       & 0                     \\
            \frac{\beta-\alpha+1}{\alpha \beta} & \frac{(\alpha-1)(\beta-\alpha+2)}{\alpha \beta} & \frac{(\alpha-1)^2}{\alpha \beta}              & 0                                        & 0                                       & 0                     \\
            0                                   & \frac{2(\beta-\alpha+j)}{\alpha\beta}           & \frac{(\alpha-2)(\beta-\alpha+4)}{\alpha\beta} & \frac{(\alpha-2)^2}{\alpha\beta}         & 0                                       & 0                     \\
            0                                   & 0                                               & \ddots                                         & \ddots                                   & \ddots                                  & 0                     \\
            0                                   & 0                                               & 0                                              & \frac{(\alpha-1)(\beta-1)}{\alpha \beta} & \frac{(\alpha-1)(\beta-1)}{\alpha\beta} & \frac{1}{\alpha\beta} \\
            0                                   & 0                                               & 0                                              & 0                                        & 1                                       & 0
        \end{array}
        \right],
\end{align}
y vemos que, en efecto, $\{X_n\}_{n \in \N_0}$ es un proceso de nacimiento y muerte.

TODO: acabar ejemplo. Polinomios de Hahn duales.
\end{comment}

\endinput