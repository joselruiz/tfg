\chapter{Análisis del problema}\label{ch:cuarto-capitulo}
    \section{Introducción}\label{sec:4.1}
    Hoy en día, es fácil encontrar software matemático con utilidades para trabajar con polinomios ortogonales. Una breve investigación basta para descubrir que las familias clásicas de polinomios ortogonales de variable continua (Hermite, Laguerre, Chebyshev, Jacobi y Legendre, entre otras) se hayan implementadas en programas como \emph{Matlab} \cite{Matlab}, \emph{Maxima} \cite{Maxima}, \emph{SageMath} \cite{SageMath} y también en bibliotecas del lenguaje \emph{Python}, tales como \emph{mpmath} \cite{mpmath}, \emph{SciPy} \cite{SciPy} y \emph{Orthopy} \cite{orthopy}.

    Por contra, ninguno de estos recursos software facilita el trabajo con las familias clásicas de polinomios ortogonales de variable discreta descritas en el \autoref{ch:segundo-capitulo}. De hecho, no se ha encontrado tal software.

    Por ello, predendemos desarrollar un software que cubra esta necesidad, que sea eficaz, eficiente, fácil de usar y accesible a toda la sociedad y, por supuesto, que se ajuste a unos estándares de calidad profesional.

    Para llevar a cabo esta tarea, hemos escogido el lenguaje de programción \emph{Python 3}. Se trata de un lenguaje de alto nivel, ampliamente utilizado en la actualidad \cite{PYPL} y muy especialmente en el ámbito científico, por la gran cantidad de bibliotecas de código que ofrece. Y, sin embargo, no tiene ninguna biblioteca para manejo de polinomios ortogonales clásicos de variable discreta.

    Así pues, hemos determinado que la mejor forma de suplir esta carencia es diseñando, desarrollando y publicando una \textbf{biblioteca de polinomios ortogonales clásicos de variable discreta}.

    Entre otras cuestiones, buscaremos la simplicidad en el diseño, la compatibilidad con la biblioteca de cálculo simbólico \emph{Sympy} \cite{sympy} y la accesibilidad al producto creado mediante su publicación en el repositorio de software para Python \emph{Pypi}. Además, la biblioteca estará orientada al uso en plataformas interactivas, como \emph{Jupyter}, aunque no será imprescindible.

    Evocando el concepto en inglés \emph{\textbf{O}rthogonal \textbf{P}olynomial \textbf{S}equence of a \textbf{Dis}crete Variable} (en castellano, Sucesión de Polinomios Ortogonales de Variable Discreta), hemos creado el acrónimo \emph{\textbf{disops}}, que será el nombre de la biblioteca.

    \section{Requisitos del software}\label{sec:4.2}
        En esta sección estableceremos qué requisitos debe satisfacer \emph{\textbf{disops}}.

        \subsection{Requisitos funcionales}
        La biblioteca \emph{\textbf{disops}} debe permitir trabajar con los polinomios de Charlier, Meixner, Krawtchouk y Hahn.  Las funcionalidades deseadas serán las mismas para las cuatro familias.

        Para simplificar la notación, los requisitos funcionales denominados de la forma RF-X.1, RF-X.2, RF-X.3, RF-X.4, se refieren, respectivamente, a las sucesiones de polinomios ortogonales de Charlier, Meixner, Krawtchouk y Hahn.

        La lista estructurada de requisitos funcionales es:
        \begin{itemize}
            \item[RF-1.] Calcular un polinomio.
                \begin{itemize}
                \item[RF-1.1.] Calcular el polinomio de grado $n$ de la sucesión de Charlier.
                \item[RF-1.2.] Calcular el polinomio de grado $n$ de la sucesión de Meixner.
                \item[RF-1.3.] Calcular el polinomio de grado $n$ de la sucesión de Krawtchouk.
                \item[RF-1.4.] Calcular el polinomio de grado $n$ de la sucesión de Hahn. 
                \end{itemize}
            \item[RF-1.] Calcular un polinomio de forma segura.\footnote{Cuando calculemos los polinomios de la SPO en cuestión, podremos utilizar alternativamente la relación de recurrencia a tres términos o la fórmula explícita. La primera opción es la más rápida y la que usaremos por defecto, pero tiene la desventaja de que progapa los errores de cálculo cometidos al calcular los polinomios de menor grado. La segunda alternativa, en general, es más lenta, ya que por la propia forma que tienen las fórmulas explícitas, con factoriales y símbolos de Pochhammer, conlleva un mayor coste computacional; pero tiene la ventaja de que es más <<segura>>, porque no propaga errores de cálculo, por lo que se le ofrecerá al usuario la posibilidad de obtener los polinomios calculados mediante la fórmula explícita si así lo desea.}
                \begin{itemize}
                \item[RF-2.1.] Calcular de forma segura el polinomio de grado $n$ de la sucesión de Charlier.
                \item[RF-2.2.] Calcular de forma segura el polinomio de grado $n$ de Meixner.
                \item[RF-2.3.] Calcular de forma segura el polinomio de grado $n$ de Krawtchouk.
                \item[RF-2.4.] Calcular de forma segura el polinomio de grado $n$ de Hahn.
                \end{itemize}
            \item[RF-3.] Calcular la sucesión de polinomios hasta grado $n$.
                \begin{itemize}
                \item[RF-3.1.] Calcular los primeros $n$ polinomios de la sucesión de Charlier.
                \item[RF-3.2.] Calcular los primeros $n$ polinomios de la sucesión de Meixner.
                \item[RF-3.3.] Calcular los primeros $n$ polinomios de la sucesión de Krawtchouk.
                \item[RF-3.4.] Calcular los primeros $n$ polinomios de la sucesión de Hahn.
                \end{itemize}
            \item[RF-4.] Calcular la sucesión de polinomios hasta grado $n$ de forma segura.
                \begin{itemize}
                \item[RF-4.1.] Calcular de forma segura los primeros $n$ polinomios de la sucesión de Charlier.
                \item[RF-4.2.] Calcular de forma segura los primeros $n$ polinomios de la sucesión de Meixner.
                \item[RF-4.3.] Calcular de forma segura los primeros $n$ polinomios de la sucesión de Krawtchouk.
                \item[RF-4.4.] Calcular de forma segura los primeros $n$ polinomios de la sucesión de Hahn.
                \end{itemize}
            \item[RF-5.] Comprobar la pertenencia de un polinomio a la sucesión. \footnote{Consideraremos que un polinomio pertenece a la sucesión cuando es múltiplo por un escalar de alguno de los polinomios de la sucesión (ver Corolario \ref{co:factor}).}
                \begin{itemize}
                \item[RF-5.1.] Comprobar la pertenencia de un polinomio dado a la sucesión de Charlier.
                \item[RF-5.2.] Comprobar la pertenencia de un polinomio dado a la sucesión de Meixner.
                \item[RF-5.3.] Comprobar la pertenencia de un polinomio dado a la sucesión de Krawtchouk.
                \item[RF-5.4.] Comprobar la pertenencia de un polinomio dado a la sucesión de Hahn.
                \end{itemize}
            \item[RF-6.] Representar gráficamente los polinomios.
                \begin{itemize}
                \item[RF-6.1] Obtener la representación gráfica de los polinomios de los grados deseados de la sucesión de Charlier.
                \item[RF-6.2] Obtener la representación gráfica de los polinomios de los grados deseados de la sucesión de Meixner.
                \item[RF-6.3] Obtener la representación gráfica de los polinomios de los grados deseados de la sucesión de Krawtchouk.
                \item[RF-6.4] Obtener la representación gráfica de los polinomios de los grados deseados de la sucesión de Hahn.
                \end{itemize}
        \end{itemize}

        \subsection{Requisitos no funcionales}
        Además de los requisitos funcionales expuestos, el software deberá satisfacer las siguientes características:
        \begin{itemize}
            \item[RN-1.] Disponibilidad al público general a través de internet.
            \item[RN-2.] Existencia de un manual de uso.
            \item[RN-3.] Compatibilidad con la biblioteca estándar de Python \emph{sympy}.
            \item[RN-4.] Rendimiento:
            \begin{itemize}
                \item[RN-4.1.] Uso inteligente de la fórmula matemática más apropiada en cada situación.
                \item[RN-4.2.] Almacenamiento de cálculos para su futura reutilización si es preciso. \footnote{Esta característica se podría considerar también como un requisito de información.}
            \end{itemize}
            \item[RN-5.] Manejo apropiado del uso incorrecto por parte de los usuarios.
        \end{itemize}
\endinput