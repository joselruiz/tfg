\chapter{Solución propuesta}\label{ch:quinto-capitulo}
    \section{Diseño del sofware}\label{sec:5.1}
    Atendiendo a los requisitos funcionales, fijada una familia de polinomios y unos parámetros concretos (lo que da lugar a una SPO), parece razonable pensar en un \emph{generador} al que solicitar la acción deseada. Por ejemplo, podemos considerar el \emph{generador de polinomios de Meixner de parámetros $\beta = 3.5$ y $c=0.1$}, o el \emph{generador de polinomios de Krawtchouk de tamaño $N=100$ y parámetro $p=0.55$}. Es a alguno de estos generadores a quien el usuario se dirigirá para demandar:
    \begin{itemize}
        \item El polinomio de grado $n$ de la sucesión. (RF-1.)
        \item El polinomio de grado $n$ de la sucesión calculado de forma segura. (RF-2.)
        \item Los primeros $n$ polinomios de la sucesión. (RF-3.)
        \item Los primeros $n$ polinomios de la sucesión calculados de forma segura. (RF-4.)
        \item Comprobar si un polinomio $p$ pertenece a la sucesión. (RF-5.)
        \item La representación gráfica de los polinomios de grados $n_1, n_2, \dots, n_k$ de la sucesión. (RF-6.)
    \end{itemize}
    
    Tendremos por tanto 4 tipos de generadores, cada uno de ellos homónimo a su correspondiente familia: \texttt{Charlier}, \texttt{Meixner}, \texttt{Krawtchouk} y \texttt{Hahn}. Podemos interpretar cada tipo de generador como una clase o tipo de dato abstracto, cuyos atributos son los parámetros propios de cada familia y los métodos, las acciones contempladas en los requisitos funcionales.

    Como las 4 clases comparten la misma estructura, las generalizamos en una clase abstracta, que llamamos \texttt{OrthogonalPolinomialsGenerator}. El diagrama de clases, que comentamos después, quedaría así:

    \begin{figure}[H]
        \includegraphics[width=\textwidth]{img/class_diagram.png}
        \caption{Diagrama de clases de \emph{disops}}
    \end{figure}

    Antes de explicar los métodos y atributos de las clases conviene hacer las siguientes observaciones:
    \begin{enumerate}
        \item Los tipos Symbol, Poly y Plot se refieren a tipos de datos abstractos definidos en la biblioteca \emph{sympy}.
        \item El símbolo \# indica que se trata de un elemento \emph{protegido}. El significado de este término es muy variable dependiendo del lenguaje de programación. Aquí, decimos que un miembro es protegido en el sentido de que es privado, salvo para clases padres e hijas.
        \item Los métodos \texttt{\_\_str\_\_}, \texttt{\_gen\_explicit}, y \texttt{\_recurrence\_coeffs} aparecen en cursiva (aunque apenas sea perceptible). Esto quiere decir que son \emph{virtuales}, en el sentido de que \texttt{OrthogonalPolinomialsGenerator} no los implementa y se prescribe su implementación a las clases hijas.
    \end{enumerate}

    A continuación comentamos los atributos y métodos del diagrama.

    \paragraph*{Constructores de las clases hijas \\}
    Reciben los parámetros propios de cada familia y un símbolo, que será la variable de los polinomios. En la implementación será opcional, pues se usará por defecto una $x$.

    \paragraph*{Atributos de \texttt{OrthogonalPolinomialsGenerator\\}}
    \begin{itemize}
        \item \texttt{params}: Lista de parámetros de la familia. Aparecen como \texttt{float} en el diagrama, pero en la implementación también admitirá símbolos.
        \item \texttt{x}: Variable de los polinomios.
        \item \texttt{generated}: Lista con los polinomios ya generados. Siempre que se calcule un polinomio, se almacena en esta variable, para no volverlo a calcular en caso de necesitarse de nuevo. En la implementación se usará un diccionario donde la clave es el grado del polinomio.
        \item \texttt{safe}: Lista de booleanos, cuyo \texttt{n}-ésimo elemento es \texttt{TRUE} si y sólo si el polinomio de grado \texttt{n} se encuentra en \texttt{generated} y fue calculado de forma segura.
    \end{itemize}

    \paragraph*{Constructor de \texttt{OrthogonalPolinomialsGenerator} \\}
    Recibe los parámetros y el símbolo.

    \paragraph*{Método \texttt{\_\_str\_\_} \\}
    Devuelve una cadena de caracteres con la información del generador de polinomos.

    \paragraph*{Método \texttt{\_gen\_explicit}\\}
    Devuelve el polinomio de grado \texttt{n} de la solución, calculado mediante la fórmula explicita de la familia en cuestión.

    \paragraph*{Método \texttt{\_recurrence\_coeffs}\\}
    Devuelve la pareja de coeficientes \texttt{(a, b)} de la relación de recurrencia a tres términos de la familia en cuestión, vista en el formato
    \begin{equation}
        P_n(x) = a P_{n-1}(x) + b P_{n-2}(x).
    \end{equation}

    \paragraph*{Método \texttt{gen\_poly}\\}
    Devuelve el polinomio de grado \texttt{n} de la sucesión. Solamente lo calcula en caso de no estar disponible en \texttt{generated}. Si tiene los dos polinomios anteriores, lo calcula mediante la relación de recurrencia, con los coeficientes proporcionados por \texttt{\_recurrence\_coeffs}, si no, llama a \texttt{\_gen\_explicit}.

    \paragraph*{Método \texttt{sgen\_poly}\\}
    Devuelve el polinomio de grado \texttt{n} de la sucesión. Lo calcula si no está en \texttt{generated} o si está pero \texttt{safe[n] = FALSE}, llamando a \texttt{\_gen\_explicit}.

    \paragraph*{Método \texttt{gen\_ops}\\}
    Devuelve los primeros \texttt{n} polinomios de la sucesión. Solamente calcula los que no estén en \texttt{generated}, mediante la relación de recurrencia, con los coeficientes proporcionados por \texttt{\_recurence\_coeffs}.

    \paragraph*{Método \texttt{sgen\_ops}\\}
    Devuelve los primeros \texttt{n} polinomios de la sucesión. Solamente calcula los que no estén en \texttt{generated} o sí estén pero su valor correspondiente en \texttt{safe} sea \texttt{FALSE}. Siempre que calcula un polinomio lo hace llamando a \texttt{\_gen\_explicit}. 

    \paragraph*{Método \texttt{check}\\}
    Comprueba si \texttt{pol} pertenece a la SPO. Si pertenece, devuelve un par formado por el grado del polinomio y la constante multiplicativa. Si no, devuelve \texttt{(-1, 0)}.

    \paragraph*{Método \texttt{plot}\\}
    Devuelve un \emph{plot} de \emph{sympy} con la gráfica de los polinomios de los grados que aparecen en la lista \texttt{degreees}. Calcula con \texttt{gen\_poly} los que necesite. El parámetro \texttt{xlim} es un par formado por los extremos izquierdo y derecho del intervalo en el que se quiere dibujar la gráfica.


    \section{Desarrollo de la implementación}\label{sec:5.2}
    El código de la biblioteca puede consultarse en un repositorio público de GitLab\footnote{\url{https://gitlab.com/joselruiz/tfg/-/tree/main/disops}}.

    \subsection{Organización del código}
    El árbol de directorios del proyecto es el siguiente:
    \begin{itemize}
        \item[] \texttt{disops}
        \begin{itemize}
            \item[] \texttt{\_\_init\_\_.py}: configuración de la biblioteca
            \item[] \texttt{Charlier.py}: clase \texttt{Charlier}
            \item[] \texttt{Hahn.py}: clase \texttt{Hahn}
            \item[] \texttt{Krawtchouk.py}: clase \texttt{Krawtchouk}
            \item[] \texttt{Meixner.py}: clase \texttt{Meixner}
            \item[] \texttt{OrthogonalPolinomialsGenerator.py}: clase \texttt{OrthogonalPolinomialsGenerator}
        \end{itemize}
        \item[] \texttt{test}
            \begin{itemize}
                \item[] \texttt{test\_disops.py}: script de Pytest
            \end{itemize}
        \item[] \texttt{LICENSE}: licencia de Software Libre (GNU GPLv3) bajo la que se publica la biblioteca.
        \item[] \texttt{Makefile}: con opciones para instalar localmente (\texttt{install}), testear mediante Pytest (\texttt{test}) y subir a Pypi (\texttt{upload}).
        \item[] \texttt{pyproject.toml}, \texttt{setup.py}: configuración de la biblioteca.
    \end{itemize}
    
    \subsection{Tests}
    Para los tests, se ha usado la herramienta Pytest. Como puede consultarse en el fichero \texttt{test\_disops.py}, los tests consisten en comprobar la coincidencia del cálculo de la sucesión con la fórmula explícita y con la recurrente, hasta un cierto grado. Esto es de vital importancia para el correcto funcionamiento de la biblioteca, y no es elemental, pues en algunos libros se exponen fórmulas de recurrencia y explícitas que no calculan los mismos polinomios, a pesar de corresponderse con la misma familia.
\endinput