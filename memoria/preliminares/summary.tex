% !TeX root = ../libro.tex
% !TeX encoding = utf8
%
%*******************************************************
% Summary
%*******************************************************

\selectlanguage{english}
\addchap{Summary}

\section*{What are orthogonal polynomials?}
According to classical linear algebra, an orthogonal basis of the vector space $\R^3$ is a basis $B = ((u_1, u_2, u_3), (v_1, v_2, v_3), (w_1, w_2, w_3 ))$ with the property that its vectors are perpendicular pairwise, with respect to the usual inner product, that is:
\begin{align}
    &(u_1, u_2, u_3)\cdot(v_1, v_2, v_3) = u_1 v_1 + u_2 v_2 + u_3 v_3 = 0, \\
    &(u_1, u_2, u_3)\cdot(w_1, w_2, w_3) = u_1 w_1 + u_2 w_2 + u_3 w_3 = 0, \\
    &(v_1, v_2, v_3)\cdot(w_1, w_2, w_3) = v_1 w_1 + v_2 w_2 + v_3 w_3 = 0.
\end{align}
For example, the canonical basis $((1, 0, 0), (0, 1, 0), (0, 0, 1))$ is an orthogonal basis.

This notion of perpendicularity can be generalized for any inner product, different from the usual one, and then we could speak of orthogonal bases with respect to the aforementioned inner product. Likewise, we can extend the concept of orthogonal basis to an arbitrary linear space, being enough to properly define a inner product.

Let us now consider the linear space of real polynomials, $\R[x]$. Since it is infinite dimensional, its bases will also be infinite, and it makes sense to denote them as polynomials sequences, say $\{P_n(x)\}_{n \in N_0}$\footnote{Ordered such that $P_n( x)$ is a polynomial of degree $n$, $\forall n \in \N_0$}. Then, defining some inner product, we can talk about orthogonal bases of $\R[x]$. We will call these bases \emph{orthogonal polynomial sequence} or, simply, \emph{orthogonal polynomials}. Depending on whether we use one inner product or another, we will obtain different orthogonal polynomial sequences.

Actually, an operation whose properties are as restrictive as  those of a inner product is not necessary to define an orthogonal polynomials sequence, a \emph{moment functional}, a more general concept that we will address in \autoref{ch:primer-capitulo}, is enough. In addition, we will study orthogonal polynomials in the complex vector space, $\C[x]$, since all their properties will also be valid for the real case.

\section*{Brief History of orthogonal polynomials}
The study of orthogonal polynomials begins with the works of Legendre (1752-1833), who in 1783 solved a problem about the attraction of an ellipsoidal body. To do this, he introduced the spherical functions, now known as \emph{Legendre polynomials}.

In the 19th Century, Chebyshev (1821-1894) and Stieltjes (1856-1894) began a general theory of orthogonal polynomials. Motivated by the resolution of physical problems, Chebyshev himself and other mathematicians, such as Jacobi (1804-1861), Hermite (1822-1901), Laguerre (1834-1888) introduced the orthogonal polynomials that today are known with their names. We are talking about those known as \emph{classical families of orthogonal polynomials of a continuous variable}; classical for being the first\footnote{And for being of a specific type, according to its rigorous definition. These families satisfy some differential properties that characterize them.}, and continuous variable, for being orthogonal with respect to inner products defined by means of an integral, an intuitive idea of continuous addition.

Later, in the first half of the 20th century, new physical problems of a discrete nature, as well as probabilistic models, led Charlier (1862-1934), Meixner (1908-1994), Hahn (1911-1998) and Krawtchouk (1892- 1942) define their respective \emph{classical families of orthogonal polynomials of a discrete variable}. These are classical, according to the formal definition that we see in \autoref{ch:segundo-capitulo}, and discrete, because they are orthogonal with respect to inner products where only the values of the polynomials in the set of natural numbers are used.

The applications of orthogonal polynomials are diverse: interpolation and approximation of functions, quadrature formulas, discretization methods, computation of moments, etc. However, these applications are not the objective of this work, which focuses on a purely mathematical study of orthogonal polynomials.

\section*{Birth and death processes}
Intuitively, a \emph{birth and death process} can be understood as a model of states, in our case discrete, where, given the current state of the model, in the next instant only its transition to an adjacent state is allowed.

More precisely, let us say that the possible states of the model are $s_0, s_1, s_2, \dots s_k$, with $k \in \N$ or $k = \infty$. If at time instant $n \in \N_0$ the process is in a certain state $s_i$, then at instant $n+1$ the model may be in one of the following states: $s_{i-1 }$, $s_i$ or $s_{i-1}$.

The first connections between orthogonal polynomials and birth and death processes were glimpsed by Wiener (1894-1964) and Ito (1915-2008) when studying the role of Hermite polynomials in integration theory. But it was not until the middle of the 20th century that Karlin (1924-2007) and McGregor (1921-1988) expressed the transition probabilities of the birth and death processes in terms of orthogonal polynomials.

Current research goes into detail about these connections, combining measurement theory, probability, spectral analysis and the theory of orthogonal polynomials itself.

\section*{Principal objectives of the work}
In the field of Mathematics, the objective of this work is to study the connection between orthogonal polynomials and processes of birth and death.To do this, we have studied the main properties of orthogonal polynomials and, in particular, the \emph{three term recurrence relation}, an equation that facilitates the aforementioned connection.

Regarding Computer Engineering, a Python library to work with the classical families of orthogonal polynomials of a discrete variable has been designed and implemented. The convenience of this software project is based on the current lack of computer tools that meet this need. In addition, the library has been published and made available to the general public.

All these objectives have been achieved.

\section*{Description and structure of the work}
The work consists of two different parts, one for each field of knowledge. The first one, about Mathematics, includes chapters \ref{ch:primer-capitulo}, \ref{ch:segundo-capitulo} and \ref{ch:tercer-capitulo}; while the second, about Computer Engineering, is made up of chapters \ref{ch:cuarto-capitulo}, \ref{ch:quinto-capitulo} and \ref{ch:sexto-capitulo}.

In \autoref{ch:primer-capitulo}, a general theory of orthogonal polynomials in the linear space of the complex polynomials $\C[x]$ is analyzed. In \autoref{sec:1.1}, we show the first rigorous definition of \emph{orthogonal polynomials sequence (OPS)}, a characterization theorem, the expression of any polynomial in terms of the orthogonal sequence and a result about the uniqueness of OPS; in \autoref{sec:1.2} we will determine under which conditions the existence of an SPO is possible; in \autoref{seccion_relacion_recurrencia_tres_terminos} we will see the \emph{three term recurrence relation}, a recursive formula that the polynomials of an OPS verifies; in \autoref{sec:1.4} we will analyze the nature of the roots of the polynomials of an OPS and the relation among the roots of different polynomials; and in \autoref{sec:1.5} we will expose the matrix representation of an OPS.

In the \autoref{ch:segundo-capitulo} we will focus on orthogonal polynomials in $\R[x]$ and the chapter development will be aimed at justifying the definition of the \emph{classical families of orthogonal polynomials of a discrete variable }. In \autoref{sec:2.1} we will distinguish between two types of OPS: continuous variable and discrete variable; in \autoref{sec:2.2} we will study a difference equation that gives rise to these classical families; and in \autoref{sec:2.3} we will see the particular cases of classical families: Charlier, Meixner, Kwratchouk and Hahn.

In the \autoref{ch:tercer-capitulo} we will deduce the link between orthogonal polynomials and processes of birth and death. \autoref{sec:3.1} is a list of basic definitions of probability theory; \autoref{sec:3.2} is about Markov processes and other associated concepts, and then gives a rigorous definition of \emph{birth and death processes (BDP)}; \autoref{sec:3.3} defines \emph{birth and death polynomials}, a particular type of OPS, the key in the connection between OPS and BDP that is manifested in the two theorems of the section; and an illustrative example of this connection is given in \autoref{sec:3.4}.

The \autoref{ch:cuarto-capitulo} is a previous analysis of the proposed Software Engineering problem. \autoref{sec:4.1} is an introduction to the problem, where we justify the need to implement a library of orthogonal polynomials of a discrete variable, we make the first decisions about the implementation, and discuss some desirable features of the software. These features are specified in \autoref{sec:4.2}, by means of a list of requirements.

In \autoref{ch:quinto-capitulo} we expose the details of the solution of the problem that has been implemented. Section ref{sec:5.1} is about conceptual design of the library and an exposition and analysis of the class diagram; and in \autoref{sec:5.2} details about the programmed code are discussed.

Finally, \autoref{ch:sexto-capitulo} is a practical showing of the use of the software programmed and published in Pypi, using a Jupyter \emph{notebook}.

\section*{Main sources consulted}
Among all the bibliographic sources consulted, the following stand out.

\cite{Chihara} and \cite{Renato}, fundamental for the theoretical exposition in the first two chapters. In \autoref{ch:segundo-capitulo} \cite{Niki} and \cite{Koekoek} are also important, the second one for getting essential formulas for the development of the library.

\cite{Patrick}, \cite{Basicsto} and \cite{Wim} have been used for basic probability concepts for the \autoref{ch:tercer-capitulo}.

The essence of the \autoref{ch:tercer-capitulo} has been extracted from \cite{Dominguez}, a recently published book.

For the development of the library, in the second part of the work, the documentation of \emph{Sympy} \cite{sympy} and \emph{Pypi}'s one \cite{pypi} have been relevant.

\selectlanguage{spanish} 
\endinput
