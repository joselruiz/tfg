% !TeX root = ../libro.tex
% !TeX encoding = utf8
%
%*******************************************************
% Introducción
%*******************************************************
% \manualmark
% \markboth{\textsc{Introducción}}{\textsc{Introducción}} 
\addchap{Introducción}

\section*{¿Qué son los polinomios ortogonales?}
De acuerdo con el álgebra lineal clásica, una base ortogonal del espacio vectorial $\R^3$ es una base $B = ((u_1, u_2, u_3), (v_1, v_2, v_3), (w_1, w_2, w_3))$ con la propiedad de que sus vectores son perperpendiculares dos a dos, según el producto escalar usual, es decir:
\begin{align}
    &(u_1, u_2, u_3)\cdot(v_1, v_2, v_3) = u_1 v_1 + u_2 v_2 + u_3 v_3 = 0, \\
    &(u_1, u_2, u_3)\cdot(w_1, w_2, w_3) = u_1 w_1 + u_2 w_2 + u_3 w_3 = 0, \\
    &(v_1, v_2, v_3)\cdot(w_1, w_2, w_3) = v_1 w_1 + v_2 w_2 + v_3 w_3 = 0.
\end{align}
Por ejemplo, la base canónica $((1, 0, 0), (0, 1, 0), (0, 0, 1))$ es una base ortogonal.

Esta noción de perpendicularidad se puede generalizar para un producto escalar cualquiera, diferente del usual, y entonces hablaríamos de bases ortogonales con respecto a dicho producto escalar. Igualmente, podemos extender el concepto de base ortogonal a un espacio vectorial arbitrario, bastando con definir apropiadamente un producto escalar.

Consideremos ahora el espacio vectorial de los polinomios reales, $\R[x]$. Como es de dimensión infinita, sus bases también lo serán, y tiene sentido denotarlas como suceciones de polinomios, digamos $\{P_n(x)\}_{n \in N_0}$\footnote{Ordenada de tal forma que $P_n(x)$ sea un polinomio de grado $n$, $\forall n \in \N_0$}. Entonces, definiendo algún producto escalar, abstracto, podremos hablar de bases ortogonales de $\R[x]$. A estas bases las denominaremos \emph{sucesiones de polinomios ortogonales} o, simplemente, \emph{polinomios ortogonales}. Según utilicemos un producto escalar u otro, obtendremos diferentes sucesiones de polinomios ortogonales.

En realidad, no es necesaria una operación con propiedades tan restrictivas como las de un producto escalar para definir una sucesión de polinomios ortogonales, es suficiente con un \emph{funcional de momentos}, un concepto más general que abordaremos en el \autoref{ch:primer-capitulo}. Además, estudiaremos polinomios ortogonales en el espacio vectorial complejo, $\C[x]$, pues todas sus propiedades serán válidas también para el caso real.

\section*{Breve historia de los polinomios ortogonales}
El estudio de los polinomios ortogonales se inicia con los trabajos de Legendre (1752-1833), quien en 1783 resolvió un problema sobre la atracción de un cuerpo elipsoidal. Para ello, introdujo las funciones esféricas, conocidas actualmente como \emph{polinomios de Legendre}. 

En el siglo XIX, Chebyshev (1821-1894) y Stieltjes (1856-1894) comenzaron una teoría general sobre polinomios ortogonales. Motivados por la resolución de problemas físicos, el propio Chebyshev y otros matemáticos, como Jacobi (1804-1861), Hermite (1822-1901), Laguerre (1834-1888) introdujeron los polinomios ortogonales que hoy en día llevan sus nombres. Hablamos de los conocidos como \emph{familias clásicas de polinomios ortogonales de variable continua}; clásicos por ser los primeros\footnote{Y por ser de un tipo concreto, según su definición rigurosa. Estas familias satisfacen ciertas propiedades diferenciales que las caracterizan.}, y de variable continua, por ser ortogonales con respecto a productos escalares definidos mediante una integral, idea intuitiva de suma continua.

Posteriormente, en la primera mitad del siglo XX, nuevos problemas físicos de naturaleza discreta, así como de modelos probabilísticos, condujeron a Charlier (1862-1934), Meixner (1908-1994), Hahn (1911-1998) y Krawtchouk (1892-1942) a definir las respectivas \emph{familias clásicas de polinomios ortogonales de variable discreta}. Estas son clásicas, según la definición formal que vemos en el \autoref{ch:segundo-capitulo}, y discretas, porque son ortogonales respecto a productos escalares en los que sólo se utilizan los valores de los polinomios en los números naturales.

Las aplicaciones de los polinomios ortogonales son diversas: interpolación y aproximación de funciones, fórmulas de cuadratura, métodos de discretización, cálculo de momentos, etc. No obstante, estas aplicaciones no son objeto de este trabajo, que se centra en un estudio matemático de los polinomios ortogonales.


\section*{Procesos de nacimiento y muerte}
Intuitivamente, un \emph{proceso de nacimiento y muerte} puede entenderse como un modelo de estados, en nuestro caso discretos, en el que, dado el estado actual del modelo, en el instante siguiente sólo se permite su transición a un estado adyacente.

Más precisamente, digamos que los posibles estados del modelo son $s_0, s_1, s_2, \dots s_k$, con $k \in \N$ ó $k = \infty$. Si en el instante de tiempo $n \in \N_0$ el proceso se encuentra en un cierto estado $s_i$, en el instante $n+1$ el modelo podrá estar en alguno de los siguientes estados: $s_{i-1}$, $s_i$ ó $s_{i-1}$.

Las primeras conexiones entre campos tan aparentemente alejados como los polinomios ortogonales y los procesos de nacimiento y muerte las vislumbraron Wiener (1894-1964) e Ito (1915-2008) al estudiar el papel de los polinomios de Hermite en la teoría de la integración. Pero no fue hasta mediados del siglo XX cuando Karlin (1924-2007) y McGregor (1921-1988) expresaron las probabilidades de transición de los procesos de nacimiento y muerte en términos de polinomios ortogonales.

Las investigaciones actuales profundizan en estas conexiones, combinando teoría de la medida, probabilidad, análisis espectral y la propia teoría de polinomios ortogonales.


\section*{Principales objetivos del trabajo}
En el ámbito de las Matemáticas, el objetivo de este trabajo es estudiar la conexión entre polinomios ortogonales y procesos de nacimiento y muerte. Para ello, hemos estudiado las principales propiedades de los polinomios ortogonales y, en particular, la relación de recurrencia a tres términos, una ecuación que facilita la citada conexión.

En cuanto a la Ingeniería Informática, se ha diseñado e implementado una biblioteca de Python para el trabajo con las familas clásicas de polinomios ortogonales de variable discreta. La conveniencia de este proyecto de software se fundamenta en la inexistencia actual de herramientas informáticas que suplan esta necesidad. Además, la biblioteca se ha publicado y está a disposición del público general.

Todos estos objetivos han sido satisfechos.


\section*{Descripción y estructura del trabajo}
El trabajo consta de dos partes diferenciadas, una para cada ámbito de conocimiento. La primera, sobre Matemáticas, abarca los capítulos \ref{ch:primer-capitulo}, \ref{ch:segundo-capitulo} y \ref{ch:tercer-capitulo}; mientras que la segunda, de Ingeniería Informática, está compuesta por los capítulos \ref{ch:cuarto-capitulo}, \ref{ch:quinto-capitulo} y \ref{ch:sexto-capitulo}.

En el \autoref{ch:primer-capitulo} se expone una teoría general de polinomios ortogonales, en el espacio vectorial de los polinomios complejos $\C[x]$. En la \autoref{sec:1.1} se da la primera definición rigurosa de \emph{sucesión de polinomios ortogonales (SPO)}, un teorema de caracterización, la expresión de un polinomio cualquiera en términos de la sucesión ortogonal y un resultado sobre la unicidad de las SPO; en la \autoref{sec:1.2} determinaremos bajo qué condiciones es posible la existencia de una SPO; en la \autoref{seccion_relacion_recurrencia_tres_terminos} veremos la \emph{relación a recurrencia a tres términos}, una fórmula recurrente que verifican los polinomios de una SPO; en la \autoref{sec:1.4} analizaremos la naturaleza de las raíces de los polinomios de una SPO y la relación entre las raíces de polinomios distintos; y en la \autoref{sec:1.5} expondremos la representación matricial de una SPO.

En el \autoref{ch:segundo-capitulo} nos centraremos en polinomios ortogonales en $\R[x]$ y su desarrollo irá encaminado a justificar la definición de las \emph{familias clásicas de polinomios ortogonales de variable discreta}. En la \autoref{sec:2.1} dintinguiremos dos tipos de SPO: de variable continua y de variable discreta; en la \autoref{sec:2.2} veremos una ecuación en diferencias que origina estas familias clásicas; y en la \autoref{sec:2.3} veremos los casos particulares de familias clásicas: Charlier, Meixner, Kwratchouk y Hahn.

En el \autoref{ch:tercer-capitulo} llegaremos a la relación entre los polinomios ortogonales y los procesos de nacimiento y muerte. La \autoref{sec:3.1} es una lista de deficiones básicas de la teoría de la probabilidad; en \autoref{sec:3.2} se tratan los procesos de Markov y otros conceptos asociados, para después dar una definición rigurosa los \emph{procesos de nacimiento y muerte (PNM)}; en la \autoref{sec:3.3} se definen los \emph{polinomios de nacimiento y muerte}, un tipo particular de SPO clave en la conexión entre SPO y PNM que se manifiesta en los dos teoremas de la sección;  y en la \autoref{sec:3.4} se expone un ejemplo ilustrativo de esta conexión.

El \autoref{ch:cuarto-capitulo} es un análisis previo del problema de Ingeniería del Software planteado. La \autoref{sec:4.1} es una introducción al problema, donde justificamos la necesidad de implementar una biblioteca de polinomios ortogonales de variable discreta, tomamos las primeras decisiones acerca de la implementación y exponemos algunas características deseables del software. Estas características las precisamos en la \autoref{sec:4.2}, mediante una lista de requisitos.

En el \autoref{ch:quinto-capitulo} exponemos los pormenores de la solución del problema que se ha llevado a cabo. La \autoref{sec:5.1} trata sobre el diseño conceptual de la biblioteca y una exposición y análisis del diagrama de clases confeccionado; y en la \autoref{sec:5.2} se comentan detalles sobre el código programado.

Finalmente, el \autoref{ch:sexto-capitulo} es una demostración práctica del uso del software programado y publicado en Pypi, mediante un \emph{notebook} de Jupyter.

\section*{Principales fuentes consultadas}
De entre todas las fuentes bibliográficas consultadas destacan las siguientes.

\cite{Chihara} y \cite{Renato}, fundamentales para la exposición teórica en los primeros dos capítulos. En el \autoref{ch:segundo-capitulo} también son importantes \cite{Niki} y \cite{Koekoek}, este último para las obtención de fórmulas imprescindibles para el desarrollo de la biblioteca.

\cite{Patrick}, \cite{Basicsto} y \cite{Wim} se han utilizado para conceptos básicos de probabilidad del \autoref{ch:tercer-capitulo}.

De \cite{Dominguez}, libro de muy reciente publicación, se ha extraído la esencia del \autoref{ch:tercer-capitulo}.

Para el desarrollo de la biblioteca, en segunda parte del trabajo, ha sido relevante la documentación de \emph{Sympy} \cite{sympy} y la de \emph{Pypi} \cite{pypi}.

\endinput
