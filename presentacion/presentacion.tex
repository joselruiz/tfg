\documentclass{beamer}

%%% paquetes
\usepackage[spanish]{babel}
\usepackage{amsmath, amsthm, amssymb}
\usepackage{mathtools}
\usepackage{verbatim}
\usepackage{tikz}
\usepackage{graphics}
\input{paquetes/comandos-entornos.tex}

%%% Tema
\usetheme{Madrid}
\usecolortheme{beaver}

%%% Portada
\title[TFG] %optional
{Polinomios Ortogonales y \\ Procesos de Nacimiento y Muerte. \\ Implementación en Python}
\subtitle{Trabajo Fin de Grado del \\ Doble Grado en Ingeniería Informática y Matemáticas}
\author[José L. Ruiz Benito] % (optional, for multiple authors)
{José Luis Ruiz Benito}
\institute[UGR] % (optional)
{
    Universidad de Granada \\
    \includegraphics[width=2cm]{img/ugr.png}   
}
\date[Curso 2021-2022] % (optional)
{Curso 2021-2022}

%%% Índice al inicio de sección
\AtBeginSection[]
{
  \begin{frame}
    \frametitle{Tabla de contenidos}
    \tableofcontents[currentsection]
  \end{frame}
}

%%%% Inicio del documento
\begin{document}
    
    %%% Diapositiva con el título
    \begin{frame}
        \titlepage
    \end{frame}

    %%% Objetivos del trabajo
    \begin{frame}{Objetivos}
        \begin{itemize}
            \item Matemáticas: Estudio de los Polinomios Ortogonales y su conexión con los Procesos de Nacimiento y Muerte.
            \item Ingeniería Informática: Diseño e implementación de una biblioteca de Polinomios Ortogonales de Variable Discreta en Python.
        \end{itemize}
    \end{frame}

    %%% Diapositiva con el índice
    \begin{frame}
        \frametitle{Tabla de contenidos}
        \tableofcontents
    \end{frame}

    \section{Parte I: Polinomios Ortogonales y Procesos de Nacimiento y Muerte}
    \subsection{Capítulo 1: Polinomios Ortogonales}
    \begin{frame}
        \frametitle{Funcionales de momentos}
        \begin{block}{Sucesión de momentos}\centering
            Sucesión de números complejos $\mun$.
        \end{block}
        \begin{block}{Funcional de momentos asociado a $\mun$}
            \begin{align*}
            \funl &: \C[x] \longrightarrow \C \\
            \funl[x^n] &= \mu_n, \quad \forall n \in \N_0.
            \end{align*}
            \begin{equation*}
                \funl[\alpha P + \beta Q] = \alpha \funl[P] + \beta  \funl[Q], \quad \forall \alpha, \beta \in \C, \quad \forall P, Q \in \C[x].  
            \end{equation*}
        \end{block}
    \end{frame}

    \begin{frame}
        \frametitle{Sucesiones de Polinomios Ortogonales (SPO)}
        \begin{block}{Sucesión de polinomios ortogonales (SPO) con respecto a $\funl$}\centering
        Sucesión de polinomios $\spol$ tal que para cualesquiera $m, n \in \N_0$:
        \begin{enumerate}
            \item $P_n$ es de grado $n$.
            \item $\funl[P_m P_n] = 0$ si $m \neq n$.
            \item $\funl[P_n^2] \neq 0$.
        \end{enumerate}
        \end{block}

        Sucesión de polinomios ortonormales (SPON): $\funl[P_n^2] = 1$.

        Sucesión de polinomios ortogonales mónicos (SPOM): $P_n$ es mónico.
    \end{frame}

    \begin{frame}{Primeras propiedades}

        \begin{block}{Teorema}
            $\spol$ SPO con respecto a $\funl$, $Q \in \C[x]$ un polinomio de grado $n$. Entonces:
            \begin{equation*}
                Q(x) = \sum_{k=0}^n \frac{\funl [Q P_k]}{\funl [P_k^2]} P_k(x)
            \end{equation*}
        \end{block}

        \begin{block}{Corolario}\centering
            Cada polinomio $P_n$ es único salvo por una constante multiplicativa.
        \end{block}
    \end{frame}

    \begin{frame}{Existencia (I)}
        Sea $\funl$ el funcional de momentos determinado por $\mun$. \\
        \vspace{5mm}

        ¿Cuándo existe una SPO con respecto a $\funl$?

        \begin{equation*}
            \Delta_n = \left|
            \begin{array}{ccccc}
              \mu_0  & \mu_1     & \mu_2     & \dots  & \mu_n     \\
              \mu_1  & \mu_2     & \mu_3     & \dots  & \mu_{n+1} \\
              \mu_2  & \mu_3     & \mu_4     & \dots  & \mu_{n+2} \\
              \vdots & \vdots    & \vdots    & \ddots & \vdots    \\
              \mu_n  & \mu_{n+1} & \mu_{n+2} & \dots  & \mu_{2n}
            \end{array}
            \right|
        \end{equation*}
        \begin{block}{Teorema}\centering
           Existe una SPO con respecto a $\funl$ $\Longleftrightarrow$ $\Delta_n \neq 0$, $\forall n \in \N_0$.
        \end{block}
    \end{frame}

    \begin{frame}{Existencia (II)}
        \begin{block}{Funcional definido positivo}
            \begin{equation*}
                \forall P \in \C[x] \setminus \{0\}, \quad P(x) \in \R_0^+, \ \forall x \in \R \quad \Longrightarrow \quad \funl[P] \in \R^+
            \end{equation*}
        \end{block}

        \begin{block}{Teorema}
            Todo funcional de momentos $\funl$ definido positivo tiene momentos $\mu_n$ reales y existe una SPOM $\{P_n\}_{n \in \N_0}$ asociada a $\funl$ compuesta por polinomios reales.
        \end{block}
    \end{frame}

    \begin{frame}{Relación de recurrencia a tres términos}
        ¿Cómo calculamos una SPO con respecto a $\funl$? $\leadsto$ Gram-Schmidt. \\

        Un método mejor:
        \begin{block}{Relación a recurrencia a tres términos}
            Sea $\{P_n\}_{n \in \N_0}$ SPO con respecto a $\funl$. Se satisface
            \begin{equation*}
                x P_n(x) = \alpha_n P_{n+1}(x) + \beta_n P_n(x) + \gamma_n P_{n-1}(x)
            \end{equation*}
            
            \begin{equation*}
                \alpha_n = \frac{\funl [x P_n P_{n+1}]}{\funl [P_{n+1}^2]}, \quad \quad
                \beta_n = \frac{\funl [x P_n P_n]}{\funl [P_n^2]}, \quad \quad
                \gamma_n = \frac{\funl [x P_n P_{n-1}]}{\funl [P_{n-1}^2]} .
            \end{equation*}
        \end{block}

        Condiciones iniciales: $P_{-1}(x) = 0$, $P_0(x) = 1$. \\

        Recíproco: Teorema de Favard.
    \end{frame}
    \begin{frame}{Matrices de Jacobi}
        \begin{align*}
            \alpha_n P_{n+1}(x) + \beta_n P_n(x) + \gamma_n P_{n-1}(x) &= x P_n(x) \\
            \left[\begin{array}{cccccccc}
                \beta_0  & \alpha_0 & 0        & 0        & 0        & \dots  \\
                \gamma_1 & \beta_1  & \alpha_1 & 0        & 0        & \ddots \\
                0        & \gamma_2 & \beta_2  & \alpha_2 & 0        & \ddots \\
                0        & 0        & \gamma_3 & \beta_3  & \alpha_3 & \ddots \\
                \vdots   & \ddots   & \ddots   & \ddots   & \ddots   & \ddots \\
            \end{array}\right]
            \left[\begin{array}{c}
                P_0(x) \\
                P_1(x) \\
                P_2(x) \\
                P_3(x) \\
                P_4(x) \\
                \vdots
            \end{array}\right]
            &= x
            \left[\begin{array}{c}
                P_0(x) \\
                P_1(x) \\
                P_2(x) \\
                P_3(x) \\
                P_4(x) \\
                \vdots
            \end{array}\right]
        \end{align*}
    \end{frame}

    \begin{frame}{Propiedades de las raíces (I)}
        \begin{block}{Soporte de $\funl$}
            Mayor conjunto $E\subset \R$ en el que $\funl$ es definido positivo:
            \begin{equation*}
                \forall P \in \C[x] \setminus \{0\}, \quad P(x) \in \R_0^+, \ \forall x \in E \quad \Longrightarrow \quad \funl[P] \in \R^+
            \end{equation*}
        \end{block}

        \begin{block}{Teorema}
            Sea $\funl$ con soporte un intervalo $I$, $\{P_n\}_{n \in \N_0}$ una SPO con respecto a $\funl$. Entonces, las raíces de $P_n$ son \textbf{reales}, \textbf{simples} y se encuentran en el \textbf{interior de $I$}, $\forall n \in \N_0$.
        \end{block}
    \end{frame}

    \begin{frame}{Propiedades de las raíces (II)}
        \begin{block}{Teorema de separación de los ceros}
            Sea $\{P_n\}_{n \in \N_0}$ una SPO asociada a $\funl$ definido positivo y $\{x_{n, 1}, x_{n, 2}, x_{n, 3} \dots, x_{n, n}\}$ el conjunto de las raíces de $P_n$, para cada $n \in \N_0$. Se verifica:
            \begin{equation*}
                x_{n+1, i} < x_{n, i} < x_{n+1, i+1}, \qquad \forall i \in \{1, 2, 3, \dots, n\}, \ \forall n \in \N_0 .
            \end{equation*}
        \end{block}
    \end{frame}

    \begin{frame}{Propiedades de las raíces (III)}
        Ejemplo:
        \begin{figure}
            \includegraphics[width=\textwidth]{img/raices_P_4.png}
        \end{figure}
    \end{frame}
    \begin{frame}{Propiedades de las raíces (IV)}
        Ejemplo:
        \begin{figure}
            \includegraphics[width=\textwidth]{img/raices_P_3.png}
        \end{figure}
    \end{frame}

    \subsection{Capítulo 2: Polinomios Ortogonales de Variable Discreta}
    \begin{frame}{Productos escalares (I)}
        A partir de ahora: $\R[x]$ y $\funl: \R[x] \longrightarrow \R$

        \begin{block}{Producto escalar en $\R[x]$}
            Aplicación $\pescalar{\cdot}{\cdot}: \R[x] \times \R[x] \longrightarrow \R$ que cumple:
            \begin{enumerate}
                \item Simetría: $\pescalar{P}{Q} = \pescalar{Q}{P}$, $\forall P, Q \in \R[x]$.
                
                \item Bilinealidad: $\pescalar{\lambda P + R}{Q} = \lambda \pescalar{P}{Q} + \pescalar{R}{Q}$, $\forall P, Q, R \in \R[x]$, $\forall \lambda \in \R$.
                
                \item Definido positivo: $\pescalar{P}{P} > 0$, $\forall P \neq 0$.
                
                \item No degenerado: Si $\pescalar{P}{P} = 0$, entonces $P = 0$.
            \end{enumerate}
        \end{block}

        \begin{block}{Producto escalar inducido por $\funl$}
            Sea $\funl$ un funcional de momentos definido positivo. La aplicación 
            \begin{equation*}
                \pescalar{P}{Q} = \funl[PQ], \quad \forall P, Q \in \R[x]
            \end{equation*}
            es un producto escalar.
        \end{block}
    \end{frame}

    \begin{frame}{Productos escalares (II)}
        \begin{block}{SPO con respecto a $\pescalar{\cdot}{\cdot}$}
           Sucesión de polinomios $\spol$ tal que para cualesquiera $m, n \in \N_0$ verifica:
            \begin{enumerate}
            \item $P_n$ es un polinomio de grado $n$.
            \item $\pescalar{P_m}{P_n} = 0$ si $m \neq n$.
            \item $\pescalar{P_n}{P_n} > 0$.
            \end{enumerate}
        \end{block}
    \end{frame}

    \begin{frame}{SPO de Variable Continua y Discreta}
        \begin{block}{SPO de Variable Continua}
            SPO con respecto a un producto escalar del tipo
            \begin{equation*}
                \pescalar{P}{Q} = \int_a^b P(x) Q(x) w(x) dx, \quad \forall P, Q \in \R[x].
            \end{equation*}
        \end{block}

        \begin{block}{SPO de Variable Discreta}
            SPO con respecto a un producto escalar del tipo
            \begin{equation*}
                \pescalar{P}{Q} = \sum_{x \in X} P(x) Q(x) w(x), \quad \forall P, Q \in \R[x],
            \end{equation*}
            con $X$ conjunto numerable.
        \end{block}
    \end{frame}

    \begin{frame}{Familias Clásicas de Variable Discreta (I)}\centering
        \begin{itemize}
            \item Charlier: $\charlier$
            \small
            \begin{equation*}
                \pescalar{P}{Q}_C = e^{-\mu} \sum_{x \in \N_0} P(x) Q(x) \frac{\mu^x}{x!}, \quad \mu \in \R^+.
            \end{equation*}
            \normalsize
            \item Meixner: $\meixner$
            \small
            \begin{equation*}
                \pescalar{P}{Q}_M = \sum_{x \in \N_0} P(x) Q(x) \frac{(\beta)_x c^x}{x!}, \quad \beta \in \R^+, c \in ]0, 1[.
            \end{equation*}
            \normalsize
            \item Krawtchouk: $\kraw$
            \small
            \begin{equation*}
                \pescalar{P}{Q}_K = \sum_{x=0}^{N} P(x)Q(x) \binom{N}{x} p^x (1-p)^{N-x}, \quad p \in ]0, 1[.
            \end{equation*}
            \normalsize
            \item Hahn: $\hahn$
            \small
            \begin{equation*}
                \pescalar{P}{Q}_h = \sum_{x=0}^{N} P(x)Q(x) \frac{\Gamma(N+\alpha-x) \Gamma(\beta+x+1)}{\Gamma(N-x) \Gamma(x+1)}, \quad \alpha, \beta \geq -1.
            \end{equation*}
            \normalsize
        \end{itemize}
    \end{frame}
    \begin{frame}{Familias Clásicas de Variable Discreta (II)}
        Polinomios de Charlier $\charlier$.\\
        \vspace{5mm}

        \begin{itemize}
        \item Relación de recurrencia a tres términos:
        \begin{equation*}\label{eq:recurrencia_charlier}
            C_{n+1}^{(\mu)}(x) = (x - n - \mu) C_n^{(\mu)}(x) - \mu n C_{n-1}^{(\mu)}(x), \quad \forall n \in \N_0.
        \end{equation*}
        Condiciones iniciales: $P_{-1}(x) = 0$, $P_0(x) = 1$.

        \item Fórmula explícita:
        \begin{equation*}\label{eq:charlier}
            C_n^{(\mu)}(x) = \sum_{k=0}^n \binom{n}{k} (x-k+1)_{k} (-\mu)^{n-k}, \quad \forall n \in \N_0.
        \end{equation*}
    \end{itemize}
    \end{frame}

    \subsection{Capítulo 3: Procesos de Nacimiento y Muerte}
    \begin{frame}{Procesos Estocásticos y de Markov}
        \begin{block}{Proceso Estocástico}\centering
           Conjunto de variables aleatorias $\pest$ sobre un espacio de probabilidad $\eprob$.

            $T$: tiempo, $S = \{X_t(\omega): \omega \in \Omega, t \in T\}$: espacio de estados.
        \end{block}

        Tomaremos $T = S = \N_0$.

        \begin{block}{Proceso de Markov}
            \small
            \begin{equation*}
                P(X_{n+1} = j \vert X_0=i_0, X_1=i_1, \dots, X_{n-1} = i_{n-1}, X_n=i) = P(X_{n+1}=j \vert X_n=i)
            \end{equation*}
            \normalsize
        \end{block}
    \end{frame}

    \begin{frame}{Probabilidades de transición}
        \begin{block}{Probabilidad de transición del estado $i$ al estado $j$}
            $$P_{i,j} = P(X_{n+1} = j \vert X_n = i)$$
        \end{block}

        \begin{block}{Matriz de Probabilidades de transición}
            \begin{equation*}
                P = \left[ P_{i,j} \right]_{i, j \in \N_0} =
                \left[
                    \begin{array}{ccccc}
                        P_{0, 0} & P_{0, 1} & P_{0, 2} & P_{0, 3} & \dots  \\
                        P_{1, 0} & P_{1, 1} & P_{1, 2} & P_{1, 3} & \dots  \\
                        P_{2, 0} & P_{2, 1} & P_{2, 2} & P_{2, 3} & \dots  \\
                        P_{3, 0} & P_{3, 1} & P_{3, 2} & P_{3, 3} & \dots  \\
                        \vdots   & \vdots   & \vdots   & \vdots   & \ddots
                    \end{array}
                    \right]
            \end{equation*}
        \end{block}
    \end{frame}

    \begin{frame}{Proceso de Nacimiento y Muerte}
        \begin{block}{Proceso de Nacimiento y Muerte PNM}
            Proceso de Markov tal que 
            \begin{equation*}
                P_{i, j} =
                \begin{cases}
                    p_i & \text{ si } j=i+1,                 \\
                    r_i & \text{ si } j=i,                   \\
                    q_i & \text{ si } j=i-1,                 \\
                    0   & \text{ si } \vert i - j \vert > 1,
                \end{cases}
            \end{equation*}
        \end{block}

        \begin{equation*}
            P =
            \left[
                \begin{array}{cccccc}
                    r_0    & p_0    & 0      & 0      & 0      & \dots  \\
                    q_1    & r_1    & p_1    & 0      & 0      & \dots  \\
                    0      & q_2    & r_2    & p_2    & 0      & \dots  \\
                    0      & 0      & q_3    & r_3    & p_3    & \dots  \\
                    \vdots & \vdots & \vdots & \ddots & \ddots & \ddots
                \end{array}
                \right]
        \end{equation*}
    \end{frame}

    \begin{frame}{Ejemplo de PNM (I)}
        \begin{figure}
            \includegraphics[width=\textwidth]{img/bolas.png}
        \end{figure}
        \begin{figure}
            \includegraphics[width=\textwidth]{img/urnas.png}
        \end{figure}
    \end{frame}
    \begin{frame}{Ejemplo de PNM (II)}\centering
        \begin{block}{Proceso}
            En cada instante de tiempo $n\in \N_0$:
            \begin{enumerate}
                \item Seleccionar una bola $k$ al azar.
                \item Cambiar de urna $k$.
            \end{enumerate}
        \end{block}

        \begin{block}{Variable aleatoria  $X_n$}
           Número de bolas en la urna $A$ en el instante $n \in \N_0$.
        \end{block}

        $X_n$ depende únicamente de $X_{n-1}$ $\Longrightarrow$ $\{X_n\}_{n \in \N_0}$ es un proceso de Markov.
    \end{frame}
    \begin{frame}{Ejemplo de PNM (III)}
        Supongamos $X_n = j$. Entonces:

        \begin{align*}
            r_j &:= P_{j, j} = P(X_{n+1} = j \vert X_n = j) = 0 \\
            p_j &:= P_{j, j+1} = P(X_{n+1} = j+1 \vert X_n = j) = \frac{2N-j}{2N}\\
            q_j &:= P_{j, j-1} = P(X_{n+1} = j-1 \vert X_n = j) = \frac{j}{2N}.
        \end{align*}

        \begin{figure}
            \includegraphics[width=\textwidth]{img/ehrenfest.jpg}
        \end{figure}
    \end{frame}
    \begin{frame}{Ejemplo de PNM (IV)}
        \begin{align*}
            P &= \left[
            \begin{array}{ccccccc}
                r_0 & p_0 & 0      & 0        & 0        & 0        \\
                q_1 & r_1 & p_1    & 0        & 0        & 0        \\
                0   & q_2 & r_2    & p_2      & 0        & 0        \\
                0   & 0   & \ddots & \ddots   & \ddots   & 0        \\
                0   & 0   & 0      & q_{2N-1} & r_{2N-1} & p_{2N-1} \\
                0   & 0   & 0      & 0        & q_{2N}   & r_{2N}
            \end{array}
            \right] \\
        &= \left[
            \begin{array}{ccccccc}
                0            & 1            & 0               & 0               & 0      & 0            \\
                \frac{1}{2N} & 0            & \frac{2N-1}{2N} & 0               & 0      & 0            \\
                0            & \frac{2}{2N} & 0               & \frac{2N-2}{2N} & 0      & 0            \\
                0            & 0            & \ddots          & \ddots          & \ddots & 0            \\
                0            & 0            & 0               & \frac{2N-1}{2N} & 0      & \frac{1}{2N} \\
                0            & 0            & 0               & 0               & 1      & 0
            \end{array}
            \right]
        \end{align*}
        $\Longrightarrow$ $\{X_n\}_{n \in \N_0}$ es un PNM
    \end{frame}
    \begin{frame}{Ejemplo de PNM (V)}
        \begin{block}{Polinomios de Krawtchouk con tamaño $2N$, $p=1/2$}
        \begin{equation*}
            \frac{2N-j}{2N} K_{j+1}(x) + \frac{j}{2N}K_{j-1}(x) = \left(1-\frac{x}{N}\right) K_j(x)
        \end{equation*}
        \end{block}

        \begin{block}{Relación matricial}
            $\mathcal K(x) = [K_0(x), K_1(x), K_2(x), \dots, K_{2N}(x)]^T$

            \begin{equation}
                P \mathcal K(x) = \left(1 - \frac{x}{N}\right) \mathcal K(x)
            \end{equation}
        \end{block}
    \end{frame}

    \section{Parte II: Implementación de una biblioteca de Polinomios Ortogonales de Variable Discreta en Python}
    \subsection{Capítulo 4: Análisis del problema}
    \begin{frame}\centering
        Software para SPO de variable continua:
        Matlab, Maxima, SageMath, Python (mpmath, SciPy, Orthopy) \\
        \vspace{5mm}
        Software para SPO de variable discreta: ¿? \\
        \vspace{1mm}
        \Huge $\Downarrow$ \\
        \vspace{1mm}
        \Huge \textbf{disops}

        \normalsize (\textbf{Dis}crete \textbf{O}rthogonal \textbf{P}olynomial \textbf{S}equence)
        \begin{figure}
            \fbox{\includegraphics[width=0.1\textwidth]{img/python.png}}
            \fbox{\includegraphics[width=0.1\textwidth]{img/pypi.png}}
            \fbox{\includegraphics[width=0.1\textwidth]{img/gitlab.png}}
            \fbox{\includegraphics[width=0.1\textwidth]{img/gpl.png}}
        \end{figure}
    \end{frame}
    \begin{frame}{Requisitos de \textbf{disops} (I)}
        \begin{block}{Requisitos funcionales}
            Para cada familia (Charlier, Meixner, Krawtchouk, Hahn):
            \begin{enumerate}
                \item Calcular el polinomio de grado $n$ de la sucesión.
                \item Calcular de forma segura el polinomio de grado $n$ de la sucesión.
                \item Calcular la sucesión de polinomios hasta grado $n$.
                \item Calcular de forma segura la sucesión de polinomios hasta grado $n$.
                \item Comprobar la pertenencia de un polinomio a la sucesión.
                \item Representar gráficamente los polinomios de la sucesión de grados $n_1, n_2, \dots, n_k$.
            \end{enumerate}
        \end{block}
    \end{frame}
    \begin{frame}{Requisitos de \textbf{disops} (II)}
        \begin{block}{Requisitos no funcionales}
            Para cada familia (Charlier, Meixner, Krawtchouk, Hahn):
            \begin{enumerate}
                \item Disponibilidad al público general a través de internet.
                \item Existencia de un manual de uso.
                \item Compatibilidad con la biblioteca estándar de Python \emph{sympy}.
                \item Rendimiento:
                \begin{enumerate}
                    \item Uso inteligente de la fórmula matemática más apropiada en cada situación.
                    \item Almacenamiento de cálculos para su futura reutilización si es preciso.
                \end{enumerate}
                \item Manejo apropiado del uso incorrecto por parte de los usuarios.
            \end{enumerate}
        \end{block}
    \end{frame}

    \subsection{Capítulo 5: Solución propuesta}
    \begin{frame}{Solución propuesta}
        \begin{figure}
            \includegraphics[width=0.75\textwidth]{img/class_diagram.png}
        \end{figure}
    \end{frame}

    \subsection{Capítulo 6: Demostración del software}
    \begin{frame}{Demostración de \textbf{disops} (I)}
        \begin{figure}
            \includegraphics[width=\textwidth]{img/demo1.png}
        \end{figure}
    \end{frame}
    \begin{frame}{Demostración de \textbf{disops} (II)}
        \begin{figure}
            \includegraphics[width=\textwidth]{img/demo2.png}
        \end{figure}
    \end{frame}
    \begin{frame}{Demostración de \textbf{disops} (III)}
        \begin{figure}
            \includegraphics[width=\textwidth]{img/demo3.png}
        \end{figure}
    \end{frame}
    \begin{frame}{Demostración de \textbf{disops} (IV)}
        \begin{figure}
            \includegraphics[width=\textwidth]{img/demo4.png}
        \end{figure}
    \end{frame}
    \begin{frame}{Demostración de \textbf{disops} (V)}
        \begin{figure}
            \includegraphics[width=\textwidth]{img/demo5.png}
        \end{figure}
    \end{frame}
    \begin{frame}{Demostración de \textbf{disops} (VI)}
        \begin{figure}
            \includegraphics[width=\textwidth]{img/demo6.png}
        \end{figure}
    \end{frame}
    \begin{frame}{Demostración de \textbf{disops} (VII)}
        \begin{figure}
            \includegraphics[width=\textwidth]{img/demo7.png}
        \end{figure}
    \end{frame}
    \begin{frame}{Demostración de \textbf{disops} (VIII)}
        \begin{figure}
            \includegraphics[width=\textwidth]{img/demo8.png}
        \end{figure}
    \end{frame}

    \begin{frame}{Conclusiones}
        \begin{itemize}
            \item Iniciación a los Polinomios Ortogonales y Procesos Estocásticos.
            \vspace{10mm}
            \item Acercamiento a la investigación en Matemáticas.
            \vspace{10mm}
            \item Realización de un proyecto profesional de Ingeniería Informática.
        \end{itemize}
    \end{frame}

    \begin{frame}{Bibliografía fundamental}
        \small
        \begin{itemize}
            \item R. Álvarez Nodarse. Polinomios hipergeométricos clásicos y q-polinomios. Monografías del seminario
            matemático García de Galdeano. Prensas Universitarias de Zaragoza, 2003.
            \item M. Domínguez de la Iglesia. Orthogonal Polynomials in the Spectral Analysis of Markov Processes.
            Encyclopedia of Mathematics and Its Applications, 181. Cambridge University Press, 2022. Birth-
            Death Models and Diffusion.
            \item A. F. Nikiforov, S. K. Suslov, and V. B. Uvarov. Classical Orthogonal Polynomials of a Discrete
Variable. Springer-Verlag, Berlin, 1991.
            \item Sympy. Sympy’s documentation. https://docs.sympy.org/. Recurso online. Accedido el
            07/06/2022.
        \end{itemize}
    \end{frame}

    \begin{frame}{Gracias por su atención}
        \begin{columns}
            \begin{column}{0.5\textwidth}
                \begin{figure}
                    \includegraphics[width=0.7\textwidth, height=0.225\textheight]{img/grafica_charlier.png}
                    \caption{Primeros $4$ polinomios de Charlier para $\mu = 4$.}
                \end{figure}
                \begin{figure}
                    \includegraphics[width=0.7\textwidth, height=0.225\textheight]{img/grafica_krawtchouk.png}
                    \caption{Polinomios de Krawtchouk para $N=7, p=0.85$.}
                \end{figure}
            \end{column}
            \begin{column}{0.5\textwidth}
                \begin{figure}
                    \includegraphics[width=0.7\textwidth, height=0.225\textheight]{img/grafica_meixner.png}
                    \caption{Primeros $6$ polinomios de Meixner para $\beta = 1, c = 0.5$.}
                \end{figure}
                \begin{figure}
                    \includegraphics[width=0.7\textwidth, height=0.225\textheight]{img/grafica_hahn.png}
                    \caption{Primeros $7$ polinomios de Hahn para $N=25, \alpha=-0.5, \beta=1.5$.}
                \end{figure}
            \end{column}
        \end{columns}
    \end{frame}
\end{document}